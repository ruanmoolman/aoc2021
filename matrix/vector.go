package matrix

type Vec []int

func (v Vec) Dot(o Vec) int {
	if len(v) != len(o) {
		panic("vectors should have the same length")
	}
	s := 0
	for i := 0; i < len(v); i++ {
		s = s + v[i]*o[i]
	}
	return s
}

func (v Vec) Equal(o Vec) bool {
	if len(v) != len(o) {
		return false
	}
	for i := 0; i < len(v); i++ {
		if v[i] != o[i] {
			return false
		}
	}
	return true
}

func (v Vec) Add(o Vec) (r Vec) {
	if len(v) != len(o) {
		panic("vectors should have the same length")
	}
	r = make(Vec, len(v))
	for i := 0; i < len(v); i++ {
		r[i] = v[i] + o[i]
	}
	return
}
func (v Vec) Sub(o Vec) (r Vec) {
	if len(v) != len(o) {
		panic("vectors should have the same length")
	}
	r = make(Vec, len(v))
	for i := 0; i < len(v); i++ {
		r[i] = v[i] - o[i]
	}
	return
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func (v Vec) ManhattanDist(o Vec) (d int) {
	if len(v) != len(o) {
		panic("vectors should have the same length")
	}
	for i := 0; i < len(v); i++ {
		d = d + abs(v[i]-o[i])
	}
	return
}
