package matrix_test

import (
	"aoc2021/matrix"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestMatrixMultiplication(t *testing.T) {
	t.Run("1x3 multiplied by 3x2", func(t *testing.T) {
		m1 := matrix.NewMatrixData([]int{1, 2, 3})
		m2 := matrix.NewMatrixData(
			[]int{1, 2},
			[]int{3, 4},
			[]int{5, 6})

		m := m1.Mul(m2)
		want := matrix.NewMatrix(1, 2)
		want.SetRow(0, []int{22, 28})
		require.Equal(t, m, want)
	})
}
