package matrix

import (
	"errors"
	"fmt"
	"strings"
)

type Operation = func(*Matrix) *Matrix

type Matrix struct {
	data []int
	c, r int
}

func (m *Matrix) String() string {
	s := make([]string, m.r)
	for r := 0; r < m.r; r++ {
		s[r] = fmt.Sprint(m.GetRow(r))
	}
	return strings.Join(s, "\n")
}

func (m *Matrix) Rows() int {
	return m.r
}
func (m *Matrix) Cols() int {
	return m.c
}
func (m *Matrix) Dims() [2]int {
	return [2]int{m.r, m.c}
}

func NewMatrix(r, c int) *Matrix {
	return &Matrix{c: c, r: r, data: make([]int, c*r)}
}

func NewMatrixData(rows ...Vec) *Matrix {
	r := len(rows)
	if r == 0 {
		panic(errors.New("empty matrix"))
	}
	c := len(rows[0])
	d := make([]int, 0)
	for _, row := range rows {
		if len(row) != c {
			panic("malformed matrix: not all rows equal length")
		}
		d = append(d, row...)
	}
	return &Matrix{c: c, r: r, data: d}
}

func (m *Matrix) Set(r, c, v int) {
	m.data[m.c*r+c] = v
}

func (m *Matrix) SetRow(r int, vs Vec) {
	for c := 0; c < m.c && c < len(vs); c++ {
		m.Set(r, c, vs[c])
	}
}

func (m *Matrix) SetCol(c int, vs Vec) {
	for r := 0; r < m.r && r < len(vs); r++ {
		m.Set(r, c, vs[r])
	}
}

func (m *Matrix) Get(r, c int) int {
	return m.data[m.c*r+c]
}
func (m *Matrix) GetRow(r int) Vec {
	return m.data[m.c*r : m.c*(r+1)]
}
func (m *Matrix) GetCol(c int) Vec {
	res := make(Vec, m.r)
	for r := 0; r < m.r; r++ {
		res[r] = m.Get(r, c)
	}
	return res
}

func (m *Matrix) Mul(o *Matrix) (res *Matrix) {
	res = NewMatrix(m.r, o.c)
	for r := 0; r < m.r; r++ {
		row := m.GetRow(r)
		for c := 0; c < o.c; c++ {
			col := o.GetCol(c)
			res.Set(r, c, row.Dot(col))
		}
	}
	return
}

func (m *Matrix) Add(o *Matrix) (res *Matrix) {
	res = NewMatrix(m.r, m.c)
	for r := 0; r < m.r; r++ {
		for c := 0; c < m.c; c++ {
			res.Set(r, c, m.Get(r, c)+o.Get(r, c))
		}
	}
	return
}
func (m *Matrix) Sub(o *Matrix) (res *Matrix) {
	res = NewMatrix(m.r, m.c)
	for r := 0; r < m.r; r++ {
		for c := 0; c < m.c; c++ {
			res.Set(r, c, m.Get(r, c)-o.Get(r, c))
		}
	}
	return
}

func (m *Matrix) Neg() (res *Matrix) {
	res = NewMatrix(m.r, m.c)
	for r := 0; r < m.r; r++ {
		for c := 0; c < m.c; c++ {
			res.Set(r, c, -m.Get(r, c))
		}
	}
	return
}

func (m *Matrix) Offset(o Vec) (res *Matrix) {
	if len(o) != m.Cols() {
		panic(errors.New("offset array must have same length as a matrix row"))
	}
	res = NewMatrix(m.r, m.c)
	for r := 0; r < m.r; r++ {
		for c := 0; c < m.c; c++ {
			res.Set(r, c, m.Get(r, c)+o[c])
		}
	}
	return
}

func Iden(n int) (res *Matrix) {
	res = NewMatrix(n, n)
	for i := 0; i < n; i++ {
		res.Set(i, i, 1)
	}
	return
}
