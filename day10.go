package main

import (
	"fmt"
	"sort"
	"strings"
)

var errCost = map[rune]int{
	')': 3,
	']': 57,
	'}': 1197,
	'>': 25137,
}
var closingMap = map[rune]rune{
	'(': ')',
	'[': ']',
	'{': '}',
	'<': '>',
}

func day10part1(data string) {
	fmt.Println("Day 10 Part 1")
	ls := strings.Split(data, "\n")

	errC := 0
lineLoop:
	for li, l := range ls {
		cs := new(RuneStack)
		for _, r := range []rune(l) {
			switch r {
			case '(', '[', '{', '<':
				cs.Push(r)
			case ')', ']', '}', '>':
				c := cs.Pop()
				v := closingMap[c]
				if v != r {
					fmt.Printf("Line %v: Want %v, got %v\n", li, string(v), string(r))
					errC = errC + errCost[r]
					continue lineLoop
				}
			}
		}
	}

	fmt.Println("Answer:", errC)
}

var runeScore = map[rune]int{
	')': 1,
	']': 2,
	'}': 3,
	'>': 4,
}

func day10part2(data string) {
	fmt.Println("Day 10 Part 2")
	ls := strings.Split(data, "\n")

	lineScores := make([]int, 0)
lineLoop:
	for _, l := range ls {
		cs := new(RuneStack)
		for _, r := range []rune(l) {
			switch r {
			case '(', '[', '{', '<':
				cs.Push(r)
			case ')', ']', '}', '>':
				c := cs.Pop()
				v := closingMap[c]
				if v != r {
					continue lineLoop
				}
			}
		}
		lineScore := 0
		for open := cs.Pop(); open != 0; open = cs.Pop() {
			c := closingMap[open]
			lineScore = lineScore * 5
			lineScore = lineScore + runeScore[c]
		}
		lineScores = append(lineScores, lineScore)
	}
	sort.Ints(lineScores)
	median := lineScores[len(lineScores)/2]
	fmt.Println("Answer:", median)
}
