package main

import (
	"aoc2021/matrix"
	"fmt"
	"regexp"
	"strings"
)

func NewPoint3D(x, y, z int) *matrix.Matrix {
	return matrix.NewMatrixData([]int{x, y, z})
}

var iden = matrix.Iden(3)
var rotXCW = matrix.NewMatrixData(
	[]int{1, 0, 0},
	[]int{0, 0, -1},
	[]int{0, 1, 0})
var rotYCW = matrix.NewMatrixData(
	[]int{0, 0, 1},
	[]int{0, 1, 0},
	[]int{-1, 0, 0})
var rotZCW = matrix.NewMatrixData(
	[]int{0, -1, 0},
	[]int{1, 0, 0},
	[]int{0, 0, 1})

func rotate(m, r *matrix.Matrix, steps int) *matrix.Matrix {
	steps = modN(steps, 4)
	res := m.Mul(iden)
	for i := 0; i < steps; i++ {
		res = res.Mul(r)
	}
	return res
}
func RotateX(m *matrix.Matrix, steps int) *matrix.Matrix {
	return rotate(m, rotXCW, steps)
}
func RotateY(m *matrix.Matrix, steps int) *matrix.Matrix {
	return rotate(m, rotYCW, steps)
}
func RotateZ(m *matrix.Matrix, steps int) *matrix.Matrix {
	return rotate(m, rotZCW, steps)
}

type Scanner struct {
	Id       int
	Beacons  *matrix.Matrix
	Position matrix.Vec
}

func (s Scanner) String() string {
	return fmt.Sprintf("Scanner #%v %v", s.Id, s.Position)
}

func (s Scanner) StringDetail() string {
	return fmt.Sprintf("Scanner #%v %v\n%v", s.Id, s.Position, s.Beacons)
}

func CountMatchingRows(ptsA, ptsB *matrix.Matrix) (count int) {
loopA:
	for ai := 0; ai < ptsA.Rows(); ai++ {
		for bi := 0; bi < ptsB.Rows(); bi++ {
			if ptsA.GetRow(ai).Equal(ptsB.GetRow(bi)) {
				count++
				continue loopA
			}
		}
	}
	return
}

func RepositionForOrigin(pts *matrix.Matrix, off matrix.Vec) *matrix.Matrix {
	newM := pts.Mul(iden)
	for i := 0; i < newM.Rows(); i++ {
		newM.SetRow(i, newM.GetRow(i).Sub(off))
	}
	return newM
}

func PointOverlap(ptsA, ptsB *matrix.Matrix) (offset matrix.Vec, count int) {
	if ptsA.Cols() != ptsA.Cols() {
		panic("cannot compare points")
	}
	// for each point in A and B, make them the origins and compare points
	for ai := 0; ai < ptsA.Rows(); ai++ {
		pA := ptsA.GetRow(ai)
		tA := RepositionForOrigin(ptsA, pA)
		for bi := 0; bi < ptsB.Rows(); bi++ {
			pB := ptsB.GetRow(bi)
			tB := RepositionForOrigin(ptsB, pB)
			matching := CountMatchingRows(tA, tB)
			if matching > 1 && matching > count {
				offset = matrix.Vec{
					pA[0] - pB[0],
					pA[1] - pB[1],
					pA[2] - pB[2]}
				count = matching
			}
		}
	}
	return
}

func Roll(m *matrix.Matrix) *matrix.Matrix {
	return RotateX(m, 1)
}
func Pitch(m *matrix.Matrix) *matrix.Matrix {
	return RotateY(m, 1)
}

func Turn(m *matrix.Matrix) *matrix.Matrix {
	return RotateZ(m, 1)
}

func applyOps(m *matrix.Matrix, ops ...matrix.Operation) *matrix.Matrix {
	m = m.Mul(iden)
	for _, op := range ops {
		m = op(m)
	}
	return m
}

func ScannerOverlap(sA, sB *Scanner) (overlaps bool) {
	var off matrix.Vec
	var c int

	opsList := [][]matrix.Operation{
		{},
		{Turn},
		{Turn, Turn},
		{Turn, Turn, Turn},
		{Pitch},
		{Pitch, Pitch, Pitch},
	}
	rollOpsList := [][]matrix.Operation{
		{},
		{Roll},
		{Roll, Roll},
		{Roll, Roll, Roll},
	}

	for _, ops := range opsList {
		for _, rollOps := range rollOpsList {
			allOps := append(ops, rollOps...)
			bRot := applyOps(sB.Beacons, allOps...)
			off, c = PointOverlap(sA.Beacons.Mul(iden), bRot)
			if c >= 12 {
				sB.Position = sA.Position.Add(off)
				sB.Beacons = bRot
				return true
			}
		}
	}
	return false
}

var pointRe = regexp.MustCompile(`^(-?\d+),(-?\d+),(-?\d+)`)

func ParsePointMatrix(s string) *matrix.Matrix {
	s = strings.TrimSpace(s)
	lines := strings.Split(s, "\n")

	m := matrix.NewMatrix(len(lines), 3)
	for r, pointStr := range lines {
		pointMatches := pointRe.FindAllStringSubmatch(pointStr, -1)
		vs := pointMatches[0]
		m.Set(r, 0, mustAtoi(vs[1]))
		m.Set(r, 1, mustAtoi(vs[2]))
		m.Set(r, 2, mustAtoi(vs[3]))
	}
	return m
}

var scannerRe = regexp.MustCompile(`--- scanner \d+ ---\s((?:-?\d+,-?\d+,-?\d+\s?)*)`)

func parseScanners(data string) []*Scanner {
	scanners := make([]*Scanner, 0)
	scannerMatches := scannerRe.FindAllStringSubmatch(data, -1)
	for i, m := range scannerMatches {
		scanner := Scanner{Id: i}
		scanner.Beacons = ParsePointMatrix(m[1])
		scanners = append(scanners, &scanner)
	}
	scanners[0].Position = matrix.Vec{0, 0, 0}
	return scanners
}

func vecListContains(l []matrix.Vec, v matrix.Vec) bool {
	for _, ls := range l {
		if ls.Equal(v) {
			return true
		}
	}
	return false
}

func getScanners(data string) []*Scanner {
	scanners := parseScanners(data)
	checkScanners := []*Scanner{scanners[0]}
	fmt.Println(scanners[0])
	for len(checkScanners) > 0 {
		sA := checkScanners[0]
		checkScanners = checkScanners[1:]
		for j := 0; j < len(scanners); j++ {
			sB := scanners[j]
			if sB.Position != nil {
				continue
			}
			if ScannerOverlap(sA, sB) {
				fmt.Println(sB)
				checkScanners = append(checkScanners, sB)
			}
		}
	}
	return scanners
}

func day19part1(data string) {
	fmt.Println("Day 19 Part 1")
	scanners := getScanners(data)
	beacons := make([]matrix.Vec, 0)
	for _, s := range scanners {
		for ri := 0; ri < s.Beacons.Rows(); ri++ {
			beacon := s.Position.Add(s.Beacons.GetRow(ri))
			if !vecListContains(beacons, beacon) {
				beacons = append(beacons, beacon)
			}
		}
	}
	fmt.Println("Answer:", len(beacons))
	fmt.Println("Part 1 Test: 79")
}

func day19part2(data string) {
	fmt.Println("Day 19 Part 2")
	scanners := getScanners(data)
	maxDist := 0
	for i := 0; i < len(scanners); i++ {
		for j := 0; j < len(scanners); j++ {
			dist := scanners[i].Position.ManhattanDist(scanners[j].Position)
			if dist > maxDist {
				maxDist = dist
			}
		}
	}
	fmt.Println("Answer:", maxDist)
	fmt.Println("Test: 3621")
}
