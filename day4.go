package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type Board struct {
	n int
	d [][]int
}

func NewBoard(n int) Board {
	b := Board{n: n, d: make([][]int, n)}
	for i := 0; i < n; i++ {
		b.d[i] = make([]int, n)
	}
	return b
}

func (b *Board) String() string {
	s := ""
	for _, l := range b.d {
		s = s + fmt.Sprintln(l)
	}
	return s
}

func (b *Board) Set(r, c, v int) {
	b.d[r][c] = v
}
func (b *Board) Get(r, c int) int {
	return b.d[r][c]
}
func (b *Board) Mark(v int) {
	for i := 0; i < b.n; i++ {
		for j := 0; j < b.n; j++ {
			if b.Get(j, i) == v {
				b.Set(j, i, -1)
				return
			}
		}
	}
}
func (b *Board) Bingo() bool {
row:
	for i := 0; i < b.n; i++ {
		for j := 0; j < b.n; j++ {
			if b.Get(i, j) != -1 {
				continue row
			}
		}
		return true
	}
col:
	for i := 0; i < b.n; i++ {
		for j := 0; j < b.n; j++ {
			if b.Get(j, i) != -1 {
				continue col
			}
		}
		return true
	}
	return false
}

func (b *Board) Remaining() int {
	s := 0
	for i := 0; i < b.n; i++ {
		for j := 0; j < b.n; j++ {
			if b.Get(i, j) != -1 {
				s = s + b.Get(i, j)
			}
		}
	}
	return s
}

func (b *Board) Check(ns []int) int {
	for ni, nv := range ns {
		b.Mark(nv)
		if b.Bingo() {
			return ni
		}
	}
	return 0
}

func readBoard(ls []string, n int) Board {
	spaceRegex, err := regexp.Compile(`\s+`)
	if err != nil {
		panic(err)
	}
	b := NewBoard(n)
	for c := 0; c < n; c++ {
		l := strings.TrimSpace(ls[c])
		vs := spaceRegex.Split(l, -1)
		for i, v := range vs {
			iv, err := strconv.Atoi(v)
			if err != nil {
				panic(err)
			}
			b.Set(c, i, iv)
		}
	}
	return b
}

func process(d string) ([]int, []Board) {
	ls := strings.Split(d, "\n")
	nums := strings.Split(ls[0], ",")
	ns := make([]int, 0, len(nums))
	for _, v := range nums {
		in, err := strconv.Atoi(v)
		if err != nil {
			panic(err)
		}
		ns = append(ns, in)
	}
	n := 5
	bs := make([]Board, 0)
	for li := 2; li < len(ls); li = li + n + 1 {
		b := readBoard(ls[li:], n)
		bs = append(bs, b)
	}
	return ns, bs
}

func day4(data string, firstLast bool) {
	numbers, boards := process(data)
	var bestCount, bestAnswer int
	for bi, b := range boards {
		bingoCount := b.Check(numbers)
		if bingoCount == 0 {
			fmt.Println("Board", bi, "couldn't get bingo")
			continue
		}
		if (firstLast && (bestCount == 0 || bingoCount < bestCount)) ||
			(!firstLast && bingoCount > bestCount) {
			fmt.Printf("Board #%v Bingod in %v\n", bi, bingoCount)
			lastNum := numbers[bingoCount]
			sumRem := b.Remaining()
			ans := lastNum * sumRem
			fmt.Println("Answer:", lastNum, "x", sumRem, "=", ans)
			fmt.Println()

			bestCount = bingoCount
			bestAnswer = ans
		}
	}
	fmt.Println("\nBest Answer:", bestAnswer)
}

func day4part1(d string) {
	fmt.Println("Day 4 Part 1")
	day4(d, true)
}

func day4part2(d string) {
	fmt.Println("Day 4 Part 2")
	day4(d, false)
}
