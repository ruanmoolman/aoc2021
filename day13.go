package main

import (
	"aoc2021/grid"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func createPaper(data string) *grid.Grid {
	parts := strings.Split(data, "\n\n")
	lines := strings.Split(parts[0], "\n")
	ps := make([]grid.Point, len(lines))
	var maxX, maxY int
	for li, l := range lines {
		xy := strings.Split(l, ",")
		x, err := strconv.Atoi(xy[0])
		if err != nil {
			panic(err)
		}
		y, err := strconv.Atoi(xy[1])
		if err != nil {
			panic(err)
		}
		ps[li] = grid.NewPoint(x, y)
		if x > maxX {
			maxX = x
		}
		if y > maxY {
			maxY = y
		}
	}
	g := grid.New(maxX+1, maxY+1)
	for _, p := range ps {
		g.SetP(p, 1)
	}
	return &g
}

type Fold struct {
	idx      int
	vertical bool
}

var foldRegex = regexp.MustCompile(`[xy]=\d+$`)

func readFolds(data string) []Fold {
	parts := strings.Split(data, "\n\n")
	lines := strings.Split(parts[1], "\n")
	folds := make([]Fold, len(lines))
	for li, l := range lines {
		res := string(foldRegex.Find([]byte(l)))
		parts := strings.Split(res, "=")
		vert := false
		if parts[0] == "y" {
			vert = true
		}
		idx, err := strconv.Atoi(parts[1])
		if err != nil {
			panic(err)
		}
		folds[li] = Fold{idx, vert}
	}
	return folds
}

func foldGrid(g *grid.Grid, f Fold) *grid.Grid {
	i := f.idx
	if f.vertical {
		newG := grid.New(g.Width(), i)
		for y := 0; y < i; y++ {
			for x := 0; x < g.Width(); x++ {
				v := g.Get(x, y)
				if v > 0 {
					newG.Set(x, y, v)
				}
			}
		}
		for y := 1; y < g.Height()-i; y++ {
			for x := 0; x < g.Width(); x++ {
				v := g.Get(x, i+y)
				if v > 0 {
					newG.Set(x, i-y, v)
				}

			}
		}
		return &newG
	} else {
		newG := grid.New(i, g.Height())
		for x := 0; x < i; x++ {
			for y := 0; y < g.Height(); y++ {
				v := g.Get(x, y)
				if v > 0 {
					newG.Set(x, y, v)
				}
			}
		}
		for x := 1; x < g.Width()-i; x++ {
			for y := 0; y < g.Height(); y++ {
				v := g.Get(i+x, y)
				if v > 0 {
					newG.Set(i-x, y, v)
				}

			}
		}
		return &newG
	}
}

func day13part1(data string) {
	fmt.Println("Day 13 Part 1")
	g := createPaper(data)
	folds := readFolds(data)

	g = foldGrid(g, folds[0])
	if g == nil {
		panic("Grid nil")
	}
	c := 0
	for _, p := range g.Points() {
		if g.GetP(p) > 0 {
			c++
		}
	}
	fmt.Println("Answer:", c)
	fmt.Println("Want: 17")
}

func day13part2(data string) {
	fmt.Println("Day 13 Part 2")
	g := createPaper(data)
	folds := readFolds(data)
	for _, f := range folds {
		g = foldGrid(g, f)
		if g == nil {
			panic("Grid nil")
		}
	}
	err := grid.SaveAsImage("output/13/part2.png", g.ToImage(10))
	if err != nil {
		fmt.Println("failed to write image:", err.Error())
	}
}
