package main

import (
	"aoc2021/grid"
	"fmt"
	"strings"
)

func DrawLines(b *grid.Grid, ls []Line, diag bool) {
	for _, l := range ls {
		switch {
		case l.isHorizontal():
			for x := min(l.x1, l.x2); x <= max(l.x1, l.x2); x++ {
				b.Set(x, l.y1, b.Get(x, l.y1)+1)
			}
		case l.isVertical():
			for y := min(l.y1, l.y2); y <= max(l.y1, l.y2); y++ {
				b.Set(l.x1, y, b.Get(l.x1, y)+1)
			}
		default:
			if !diag {
				continue
			}
			xdel := 1
			if l.x2 < l.x1 {
				xdel = -1
			}
			ydel := 1
			if l.y2 < l.y1 {
				ydel = -1
			}
			x := l.x1
			y := l.y1
			for ((xdel == 1 && x <= l.x2) || (xdel == -1 && x >= l.x2)) &&
				((ydel == 1 && y <= l.y2) || (ydel == -1 && y >= l.y2)) {
				b.Set(x, y, b.Get(x, y)+1)
				x = x + xdel
				y = y + ydel
			}
		}
	}
}

type Line struct {
	x1 int
	y1 int
	x2 int
	y2 int
}

func (s Line) isHorizontal() bool {
	return s.y1 == s.y2
}
func (s Line) isVertical() bool {
	return s.x1 == s.x2
}

func getLines(data string) []Line {
	ls := strings.Split(data, "\n")
	lines := make([]Line, 0)
	for li, l := range ls {
		if l == "" {
			continue
		}
		ps := strings.Split(l, " -> ")
		if len(ps) < 2 {
			panic(fmt.Errorf("Too few points on line #%v", li))
		}
		xy1, err := toInts(strings.Split(ps[0], ","))
		if err != nil {
			panic(err)
		}
		xy2, err := toInts(strings.Split(ps[1], ","))
		if err != nil {
			panic(err)
		}
		lines = append(lines, Line{x1: xy1[0], y1: xy1[1], x2: xy2[0], y2: xy2[1]})
	}
	return lines
}

func countDanger(g grid.Grid) int {
	c := 0
	for _, p := range g.Points() {
		if g.GetP(p) >= 2 {
			c++
		}
	}
	return c
}

func day5(data string, diag bool) {
	fmt.Println("Day 5 Part 1")
	lines := getLines(data)
	fmt.Println("Lines:", lines)
	g := grid.NewSquare(1000)
	DrawLines(&g, lines, diag)
	err := grid.SaveAsImage("~/aoc2021/output/5/output.png", g.ToImage(1))
	if err != nil {
		fmt.Println("Failed writing image:", err.Error())
	}
	fmt.Println("Answer:", countDanger(g))
}

func day5part1(data string) {
	fmt.Println("Day 5 Part 1")
	day5(data, false)
}

func day5part2(data string) {
	fmt.Println("Day 5 Part 2")
	day5(data, true)
}
