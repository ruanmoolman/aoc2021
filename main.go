package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func readData(day string, name string) (string, error) {
	dayParts := strings.Split(day, "-")
	fp := filepath.Join("input", dayParts[0], name)
	data, err := ioutil.ReadFile(fp)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func arg(name string, i int) (string, error) {
	if len(os.Args) < i+1 {
		return "", fmt.Errorf("expecting `%v` argument\n", name)
	}
	return os.Args[i], nil
}

func exec(day, part, file string) {

	fmt.Println("Advent of Code 2021!!")

	var fn func(string)
	switch day + "-" + part {
	case "1-1":
		fn = day1part1
	case "1-2":
		fn = day1part2
	case "2-1":
		fn = day2part1
	case "2-2":
		fn = day2part2
	case "3-1":
		fn = day3part1
	case "3-2":
		fn = day3part2
	case "4-1":
		fn = day4part1
	case "4-2":
		fn = day4part2
	case "5-1":
		fn = day5part1
	case "5-2":
		fn = day5part2
	case "6-1":
		fn = day6part1
	case "6-2":
		fn = day6part2
	case "7-1":
		fn = day7part1
	case "7-2":
		fn = day7part2
	case "8-1":
		fn = day8part1
	case "8-2":
		fn = day8part2
	case "9-1":
		fn = day9part1
	case "9-2":
		fn = day9part2
	case "10-1":
		fn = day10part1
	case "10-2":
		fn = day10part2
	case "11-1":
		fn = day11part1
	case "11-2":
		fn = day11part2
	case "11-gif":
		fn = day11gif
	case "12-1":
		fn = day12part1
	case "12-2":
		fn = day12part2
	case "13-1":
		fn = day13part1
	case "13-2":
		fn = day13part2
	case "14-1":
		fn = day14part1
	case "14-2":
		fn = day14part2
	case "15-1":
		fn = day15part1
	case "15-2":
		fn = day15part2
	case "16-1":
		fn = day16part1
	case "16-2":
		fn = day16part2
	case "17-1":
		fn = day17part1
	case "17-2":
		fn = day17part2
	case "18-1":
		fn = day18part1
	case "18-2":
		fn = day18part2
	case "19-1":
		fn = day19part1
	case "19-2":
		fn = day19part2
	case "20-1":
		fn = day20part1
	case "20-2":
		fn = day20part2
	case "20-gif":
		fn = day20gif
	case "21-1":
		fn = day21part1
	case "21-2":
		fn = day21part2
	case "22-1":
		fn = day22part1
	case "22-2":
		fn = day22part2
	case "23-1":
		fn = day23part1
	case "23-2":
		fn = day23part2
	case "24-1":
		fn = day24part1
	case "24-2":
		fn = day24part2
	case "25-1":
		fn = day25part1
	case "25-2":
		fn = day25part2
	default:
		fmt.Printf("day `%v` part `%v` not supported\n", day, part)
		return
	}

	if !endsWith(file, ".txt") {
		file = file + ".txt"
	}
	data, err := readData(day, file)
	if err != nil {
		fmt.Println("failed to read data:", err.Error())
		return
	}
	fn(data)
}

func main() {
	for _, a := range os.Args {
		if a == "-h" || a == "--help" {
			fmt.Println("\nAdvent of Code 2021!\n\n" +
				"USAGE:\n" +
				"  aoc2021 [OPTIONS] DAY PART FILE\n\n" +
				"OPTIONS:\n" +
				"  --help, -h  show help (default: false)")
			return
		}
	}

	day, err := arg("day", 1)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	part, err := arg("day", 2)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	file, err := arg("input file name", 3)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	exec(day, part, file)
}
