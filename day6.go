package main

import (
	"fmt"
	"strings"
)

func tick(fc []int) []int {
	nfc := make([]int, len(fc))
	nf := fc[0]
	for i := 1; i < len(nfc); i++ {
		nfc[i-1] = fc[i]
	}
	nfc[6] = nfc[6] + nf
	nfc[8] = nfc[8] + nf
	return nfc
}

func run(data string, days int) int {
	fs, err := toInts(strings.Split(data, ","))
	if err != nil {
		panic(err)
	}

	fc := make([]int, 9)
	for _, v := range fs {
		fc[v]++
	}
	for day := 1; day <= days; day++ {
		fc = tick(fc)
	}
	return sum(fc)
}

func runOpt(data string, days int) int {
	fs, err := toInts(strings.Split(data, ","))
	if err != nil {
		panic(err)
	}

	b := NewCircularBuffer(9)
	for _, v := range fs {
		b.Set(v, b.Get(v)+1)
	}
	for day := 1; day <= days; day++ {
		b.Set(-2, b.Get(-2)+b.Get(0))
		b.Step()
	}
	return b.Sum()
}

func day6part1(data string) {
	fmt.Println("Day 6 Part 1")
	fmt.Println("Answer: ", runOpt(data, 80))
}

func day6part2(data string) {
	fmt.Println("Day 6 Part 2")
	fmt.Println("Answer: ", runOpt(data, 256))
}
