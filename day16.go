package main

import (
	"errors"
	"fmt"
	"math"
	"strconv"
)

type Tokenizer struct {
	data string
	i    int
	next string
}

func NewTokenizer(d string) *Tokenizer {
	return &Tokenizer{data: d, i: 0, next: ""}
}

func (t *Tokenizer) rest() string {
	if t.i >= len(t.data) {
		return ""
	}
	return t.data[t.i:]
}

func (t *Tokenizer) EatStr(n int) string {
	rest := t.rest()
	if n > len(rest) {
		return ""
	}
	s := rest[:n]
	t.i = t.i + n
	return s
}

func (t *Tokenizer) EatInt(n int) int {
	s := t.EatStr(n)
	if len(s) == 0 {
		return -1
	}
	return BinToInt(s)
}

func BinToInt(b string) int {
	v, err := strconv.ParseInt(b, 2, 64)
	if err != nil {
		panic(err)
	}
	return int(v)
}

func HexToBin(h string) string {
	b := ""
	for i := 0; i < len(h); i++ {
		v, err := strconv.ParseInt(h[i:i+1], 16, 64)
		if err != nil {
			panic(err)
		}
		p := strconv.FormatInt(v, 2)
		for len(p) < 4 {
			p = "0" + p
		}
		b = b + p
	}
	return b
}

type PacketKind int

const (
	LiteralPacket PacketKind = 4

	SumOperation         = 0
	ProdOperation        = 1
	MinimumOperation     = 2
	MaximumOperation     = 3
	GreaterThenOperation = 5
	LessThenOperation    = 6
	EqualOperation       = 7
)

type Packet struct {
	version int
	kind    PacketKind
	sub     []*Packet
	value   int
}

func (p *Packet) String() string {
	switch p.kind {
	case LiteralPacket:
		return fmt.Sprintf("L[%v]:%v", p.version, p.value)
	default:
		return fmt.Sprintf("O[%v]:[%v]", p.version, p.sub)
	}
}

func decodeLiteralPacket(v int, k PacketKind, t *Tokenizer) *Packet {
	more := t.EatInt(1) > 0
	s := ""
	for {
		s = s + t.EatStr(4)
		if !more {
			break
		}
		more = t.EatInt(1) > 0
	}
	return &Packet{version: v, kind: k, value: BinToInt(s)}
}

func decodeSubPackets(s string) []*Packet {
	t := NewTokenizer(s)
	ps := make([]*Packet, 0)
	var p *Packet
	for {
		p = decodePacket(t)
		ps = append(ps, p)
		if t.rest() == "" {
			return ps
		}
	}

}

func decodeOperatorPacket(v int, k PacketKind, t *Tokenizer) *Packet {
	l := t.EatInt(1)
	var ps []*Packet
	if l > 0 {
		numSubs := t.EatInt(11)
		for si := 0; si < numSubs; si++ {
			ps = append(ps, decodePacket(t))
		}
	} else {
		bits := t.EatInt(15)
		ps = decodeSubPackets(t.EatStr(bits))
	}

	return &Packet{version: v, kind: k, sub: ps}
}

func decodePacket(t *Tokenizer) *Packet {
	v := t.EatInt(3)
	k := PacketKind(t.EatInt(3))
	switch k {
	case LiteralPacket:
		return decodeLiteralPacket(v, k, t)
	default:
		return decodeOperatorPacket(v, k, t)
	}
}

func DecodeMessage(data string) *Packet {
	t := NewTokenizer(HexToBin(data))
	return decodePacket(t)
}

func countPacketVersions(ps ...*Packet) int {
	c := 0
	for _, p := range ps {
		c = c + p.version
		if p.kind != LiteralPacket {
			c = c + countPacketVersions(p.sub...)
		}
	}
	return c
}

func EvalPackage(p *Packet) int {
	switch p.kind {
	case SumOperation:
		s := 0
		for _, sp := range p.sub {
			s = s + EvalPackage(sp)
		}
		return s
	case ProdOperation:
		s := 1
		for _, sp := range p.sub {
			s = s * EvalPackage(sp)
		}
		return s
	case MinimumOperation:
		s := math.MaxInt64
		for _, sp := range p.sub {
			s = min(s, EvalPackage(sp))
		}
		return s
	case MaximumOperation:
		s := math.MinInt64
		for _, sp := range p.sub {
			s = max(s, EvalPackage(sp))
		}
		return s
	case LessThenOperation:
		if EvalPackage(p.sub[0]) < EvalPackage(p.sub[1]) {
			return 1
		} else {
			return 0
		}
	case GreaterThenOperation:
		if EvalPackage(p.sub[0]) > EvalPackage(p.sub[1]) {
			return 1
		} else {
			return 0
		}
	case EqualOperation:
		if EvalPackage(p.sub[0]) == EvalPackage(p.sub[1]) {
			return 1
		} else {
			return 0
		}
	case LiteralPacket:
		return p.value
	default:
		panic(errors.New("operation not implemented"))
	}
}

func day16part1(data string) {
	fmt.Println("Day 16 Part 1")
	p := DecodeMessage(data)
	fmt.Println("Answer:", countPacketVersions(p))
}

func day16part2(data string) {
	fmt.Println("Day 16 Part 2")
	p := DecodeMessage(data)
	fmt.Println("Answer:", EvalPackage(p))
}
