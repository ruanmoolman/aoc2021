package main

import (
	"io/ioutil"
	"testing"
)

func BenchmarkDay14Part2(b *testing.B) {
	fbuf, err := ioutil.ReadFile("input/14/main.txt")
	input := string(fbuf)
	if err != nil {
		b.Fatalf("error reading file: %v", err.Error())
	}
	for n := 0; n < b.N; n++ {
		_day14part2(input, 40)
	}
}
