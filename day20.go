package main

import (
	"aoc2021/grid"
	"fmt"
	"image"
	"strconv"
	"strings"
)

type Encoding = []int
type InfImage struct {
	g      *grid.Grid
	infVal int
}

func (ii InfImage) String() string {
	return fmt.Sprintf("%vINF:%v", *ii.g, ii.infVal)
}

func parseImageEncoding(data string) Encoding {
	lines := strings.Split(data, "\n")
	e := make(Encoding, len(lines[0]))
	for i, v := range lines[0] {
		if v == '#' {
			e[i] = 1
		}
	}
	return e
}

func parseImageData(data string) *InfImage {
	lines := strings.Split(data, "\n")
	imgLines := lines[2:]
	g := grid.New(len(imgLines[0]), len(imgLines))
	for ri, l := range imgLines {
		for ci, c := range l {
			if c == '#' {
				g.Set(ci, ri, 1)
			}
		}
	}
	return &InfImage{&g, 0}
}

func addBorder(g *grid.Grid, v int) *grid.Grid {
	newG := grid.New(g.Width()+2, g.Height()+2)
	lastX, lastY := newG.Width()-1, newG.Height()-1
	for x := 0; x < newG.Width(); x++ {
		newG.Set(x, 0, v)
		newG.Set(x, lastY, v)
	}
	for y := 0; y < newG.Height(); y++ {
		newG.Set(0, y, v)
		newG.Set(lastX, y, v)
	}
	for x := 0; x < g.Width(); x++ {
		for y := 0; y < g.Height(); y++ {
			newG.Set(x+1, y+1, g.Get(x, y))
		}
	}
	return &newG
}

func getEncodingValue(e Encoding, binStr string) int {
	eIdx, err := strconv.ParseInt(binStr, 2, 64)
	if err != nil {
		panic(err)
	}
	return e[eIdx]
}

func decodePixel(x, y int, g *grid.Grid, e Encoding, def int) int {
	binStr := ""
	for yi := y - 1; yi <= y+1; yi++ {
		for xi := x - 1; xi <= x+1; xi++ {
			binStr = binStr + fmt.Sprint(g.GetSafe(xi, yi, def))
		}
	}
	return getEncodingValue(e, binStr)
}

func enhanceImage(ii *InfImage, e Encoding) {
	g := ii.g
	g = addBorder(g, ii.infVal)
	newG := grid.New(g.Width(), g.Height())
	for x := 0; x < newG.Width(); x++ {
		for y := 0; y < newG.Height(); y++ {
			newG.Set(x, y, decodePixel(x, y, g, e, ii.infVal))
		}
	}
	ii.g = &newG
	if ii.infVal == 0 {
		ii.infVal = getEncodingValue(e, "000000000")
	} else {
		ii.infVal = getEncodingValue(e, "111111111")
	}
}

func gridNonZero(g *grid.Grid) int {
	s := 0
	for _, p := range g.Points() {
		if g.GetP(p) > 0 {
			s++
		}
	}
	return s
}
func day20part1(data string) {
	fmt.Println("Day 20 Part 1")
	e := parseImageEncoding(data)
	ii := parseImageData(data)
	enhanceImage(ii, e)
	enhanceImage(ii, e)

	fmt.Println("Answer:", gridNonZero(ii.g))
	fmt.Println("Test: 35")
}

func day20part2(data string) {
	fmt.Println("Day 20 Part 2")
	e := parseImageEncoding(data)
	ii := parseImageData(data)
	for i := 0; i < 50; i++ {
		enhanceImage(ii, e)
	}
	fmt.Println("Answer:", gridNonZero(ii.g))
	fmt.Println("Test: 3351")
}

func imageCentered(ii *InfImage, g *grid.Grid) {
	for _, p := range g.Points() {
		g.SetP(p, ii.infVal)
	}
	off := grid.NewPoint(
		(g.Width()-ii.g.Width())/2,
		(g.Height()-ii.g.Height())/2)
	for _, p := range ii.g.Points() {
		g.SetP(p.Add(off), ii.g.GetP(p))
	}
}

func day20gif(data string) {
	fmt.Println("Day 20 gif")

	steps := 50

	e := parseImageEncoding(data)
	ii := parseImageData(data)
	for i := 0; i < steps; i++ {
		enhanceImage(ii, e)
	}

	g := grid.New(ii.g.Width(), ii.g.Height())
	// Reinitialize for the gif creation
	ii = parseImageData(data)

	imgs := make([]*image.Paletted, 0)
	imageCentered(ii, &g)
	imgs = append(imgs, g.ToPaletted(10))
	for i := 0; i < steps; i++ {
		fmt.Printf("%v ", i)
		enhanceImage(ii, e)
		imageCentered(ii, &g)
		imgs = append(imgs, g.ToPaletted(10))
	}
	fmt.Println("...done")

	err := grid.SaveAsGif("output/20/main.gif", imgs)
	if err != nil {
		fmt.Println("failed to save gif")
	}
	fmt.Println("saved gif successfully")
}
