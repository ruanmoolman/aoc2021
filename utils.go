package main

import (
	"strconv"
	"strings"
)

func filterStrings(ss []string, ff func(string) bool) []string {
	ns := make([]string, 0)
	for _, s := range ss {
		if ff(s) {
			ns = append(ns, s)
		}
	}
	return ns
}

func mustAtoi(s string) int {
	v, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return v
}

func toInts(ss []string) ([]int, error) {
	is := make([]int, 0, len(ss))
	for _, v := range ss {
		i, err := strconv.Atoi(v)
		if err != nil {
			return nil, err
		}
		is = append(is, i)
	}
	return is, nil
}

// lerp interpolates between a and b based on the value of r
// eg. r=0 would return a and r=1 would return b
func lerp(a, b, r float64) float64 {
	return a + r*(b-a)
}

func sum(is []int) int {
	s := 0
	for _, v := range is {
		s = s + v
	}
	return s
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

// modN handles negative values
func modN(v, m int) int {
	return (v%m + m) % m
}

// endsWith returns true if the ending of `s` matches `e`
func endsWith(s, e string) bool {
	return len(s) >= len(e) && s[len(s)-len(e):] == e
}

// strDiff returns string `a` without all the runes that also appear in `b`
func strDiff(a, b string) string {
	s := ""
	for _, r := range []rune(a) {
		if !strings.ContainsRune(b, r) {
			s = s + string(r)
		}
	}
	return s
}

// dimSum calculates the sum of the v + v-1 + ... + v-v
func dimSum(v int) int {
	return v * (v + 1) / 2
}
