package main_test

import (
	"github.com/stretchr/testify/require"
	"testing"
)
import aoc "aoc2021"

func TestCuboidOverlap(t *testing.T) {
	cases := []struct {
		name string
		a, b *aoc.Cuboid
		want *aoc.Cuboid
	}{
		{"non-overlapping returns nil",
			&aoc.Cuboid{2, 4, 2, 4, 2, 4},
			&aoc.Cuboid{0, 1, 0, 1, 0, 1},
			nil},
		{"overlapping corner",
			&aoc.Cuboid{-3, 3, -5, 5, -10, 10},
			&aoc.Cuboid{1, 5, -10, 2, 1, 12},
			&aoc.Cuboid{1, 3, -5, 2, 1, 10},
		},
		{"completely overlaps",
			&aoc.Cuboid{-1, 1, -1, 1, -1, 1},
			&aoc.Cuboid{-2, 2, -2, 2, -2, 2},
			&aoc.Cuboid{-1, 1, -1, 1, -1, 1},
		},
	}

	for _, c := range cases {
		got := c.a.Overlap(c.b)
		require.Equal(t, c.want, got)
	}
}

func TestCuboidSplit(t *testing.T) {
	testCuboid := &aoc.Cuboid{-1, 1, 0, 0, 0, 0}
	cases := []struct {
		name      string
		low, high int
		want      []*aoc.Cuboid
	}{
		{"outside right", 2, 3, []*aoc.Cuboid{testCuboid}},
		{"overlap", -2, 2, []*aoc.Cuboid{testCuboid}},
		{"overlap", -1, 1, []*aoc.Cuboid{testCuboid}},
		{"outside left", -3, -2, []*aoc.Cuboid{testCuboid}},
		{"right", 0, 2,
			[]*aoc.Cuboid{{-1, -1, 0, 0, 0, 0}, {0, 1, 0, 0, 0, 0}}},
		{"left", -2, 0,
			[]*aoc.Cuboid{{-1, 0, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0}}},
		{"inside", 0, 0,
			[]*aoc.Cuboid{{-1, -1, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0},
				{1, 1, 0, 0, 0, 0}}},
		{"inside against left side", -1, 0,
			[]*aoc.Cuboid{{-1, 0, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0}}},
		{"inside against right side", 0, 1,
			[]*aoc.Cuboid{{-1, -1, 0, 0, 0, 0}, {0, 1, 0, 0, 0, 0}}},
	}

	for _, c := range cases {
		got := testCuboid.Split(0, c.low, c.high)
		require.Equal(t, c.want, got)
	}
}

func TestCuboidRemoveOverlapping(t *testing.T) {
	cases := []struct {
		name   string
		cube   *aoc.Cuboid
		remove *aoc.Cuboid
		want   []*aoc.Cuboid
	}{
		{"remove half",
			&aoc.Cuboid{0, 2, 0, 2, 0, 2},
			&aoc.Cuboid{1, 2, 0, 2, 0, 2},
			[]*aoc.Cuboid{{0, 0, 0, 2, 0, 2}},
		},
		{"remove all",
			&aoc.Cuboid{0, 2, 0, 2, 0, 2},
			&aoc.Cuboid{0, 2, 0, 2, 0, 2},
			[]*aoc.Cuboid{},
		},
		{"remove corner",
			&aoc.Cuboid{0, 2, 0, 2, 0, 2},
			&aoc.Cuboid{2, 2, 2, 2, 2, 2},
			[]*aoc.Cuboid{
				{0, 1, 0, 1, 0, 1},
				{0, 1, 0, 1, 2, 2},
				{0, 1, 2, 2, 0, 1},
				{0, 1, 2, 2, 2, 2},
				{2, 2, 0, 1, 0, 1},
				{2, 2, 0, 1, 2, 2},
				{2, 2, 2, 2, 0, 1},
			},
		},
	}

	for _, c := range cases {
		got := aoc.RemoveOverlapping(c.cube, c.remove)
		require.Equal(t, c.want, got)
	}
}

func TestCuboidVolume(t *testing.T) {
	cases := []struct {
		cube *aoc.Cuboid
		want int
	}{
		{&aoc.Cuboid{0, 0, 0, 0, 0, 0}, 1},
		{&aoc.Cuboid{0, 2, 0, 2, 0, 2}, 27},
		{&aoc.Cuboid{0, 9, -10, -1, -1, 1}, 300},
	}

	for _, c := range cases {
		got := c.cube.Volume()
		require.Equal(t, c.want, got)
	}
}
