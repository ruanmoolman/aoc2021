package main

import (
	"fmt"
	"testing"
)

func TestModN(t *testing.T) {
	m := 5
	in := []int{5, 2, -1, -4, -10}
	out := []int{0, 2, 4, 1, 0}

	for i := 0; i < len(in); i++ {
		t.Run(fmt.Sprintf("modN(%v, %v)", in[i], m), func(t *testing.T) {
			res := modN(in[i], m)
			if out[i] != res {
				t.Fatalf("Expected modN(%v, %v)=%v, got %v", in[i], m, out[i], res)
			}
		})
	}
}
