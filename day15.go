package main

import (
	"aoc2021/grid"
	"fmt"
)

func findBestPath(g *grid.Grid) int {
	costG := grid.New(g.Width(), g.Height())
	checkQ := PointQueue{}
	checkQ.Push(grid.NewPoint(0, 0))
	for p, ok := checkQ.Pop(); ok; p, ok = checkQ.Pop() {
		pCost := costG.GetP(p)
		for _, offset := range grid.OrthogonalOffsets {
			op := p.Add(offset)
			opVal := g.GetPSafe(op, -1)
			if opVal < 0 {
				continue
			}
			opCost := costG.GetP(op)
			moveCost := pCost + opVal
			if opCost <= 0 || moveCost < opCost {
				checkQ.Push(op)
				costG.SetP(op, moveCost)
			}
		}
	}
	return costG.Get(costG.Width()-1, costG.Height()-1)
}

func day15part1(data string) {
	fmt.Println("Day 15 Part 1")
	g := gridFromString(data)
	fmt.Println(g)
	fmt.Println("Answer:", findBestPath(&g))
}

func enlargeMap(g *grid.Grid, mul int) grid.Grid {
	largeG := grid.New(g.Width()*mul, g.Height()*mul)
	for y := 0; y < mul; y++ {
		for x := 0; x < mul; x++ {
			addCost := y + x
			offset := grid.NewPoint(x*g.Width(), y*g.Height())
			for _, p := range g.Points() {
				val := g.GetP(p) + addCost
				for val > 9 {
					val = val - 9
				}
				largeG.SetP(offset.Add(p), val)
			}
		}
	}
	return largeG
}

func day15part2(data string) {
	fmt.Println("Day 15 Part 2")
	g := gridFromString(data)
	g = enlargeMap(&g, 5)
	fmt.Println("Answer:", findBestPath(&g))
}
