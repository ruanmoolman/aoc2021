package main_test

import (
	main "aoc2021"
	"testing"
)

func TestEvalPackage(t *testing.T) {
	cases := []struct {
		name, input string
		want        int
	}{
		{"sum", "C200B40A82", 3},                   // finds the sum of 1 and 2, resulting in the value 3.
		{"product", "04005AC33890", 54},            // finds the product of 6 and 9, resulting in the value 54.
		{"minimum", "880086C3E88112", 7},           // finds the minimum of 7, 8, and 9, resulting in the value 7.
		{"maximum", "CE00C43D881120", 9},           // finds the maximum of 7, 8, and 9, resulting in the value 9.
		{"less than", "D8005AC2A8F0", 1},           // produces 1, because 5 is less than 15.
		{"greater than", "F600BC2D8F", 0},          // produces 0, because 5 is not greater than 15.
		{"not equal", "9C005AC2F8F0", 0},           // produces 0, because 5 is not equal to 15.
		{"equal", "9C0141080250320F1802104A08", 1}, // produces 1, because 1 + 3 = 2 * 2.
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := main.EvalPackage(main.DecodeMessage(c.input))
			if c.want != got {
				t.Fatalf("Wanted %v, got %v", c.want, got)
			}
		})
	}
}
