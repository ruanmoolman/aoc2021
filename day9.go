package main

import (
	"aoc2021/grid"
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"
)

func getGrid(data string) grid.Grid {
	rows := strings.Split(data, "\n")
	g := grid.New(len(rows[0]), len(rows))

	for y := 0; y < g.Height(); y++ {
		for x := 0; x < g.Width(); x++ {
			i, err := strconv.Atoi(string(rows[y][x]))
			if err != nil {
				panic(err)
			}
			g.Set(x, y, i)
		}
	}
	return g
}

func getLowPoints(g *grid.Grid) []grid.Point {
	lowPoints := make([]grid.Point, 0)
	for y := 0; y < g.Height(); y++ {
	point:
		for x := 0; x < g.Width(); x++ {
			p := grid.NewPoint(x, y)
			v := g.GetP(p)
			for _, o := range grid.OrthogonalOffsets {
				ov := g.GetPSafe(p.Add(o), math.MaxInt64)
				if v >= ov {
					continue point
				}
			}
			lowPoints = append(lowPoints, grid.NewPoint(x, y))
		}
	}
	return lowPoints
}

func day9part1(data string) {
	fmt.Println("Day 9 Part 1")
	g := getGrid(data)
	lp := getLowPoints(&g)

	risk := 0
	for _, p := range lp {
		risk = risk + g.GetP(p) + 1
	}

	fmt.Println("Answer:", risk)
}

func pointListContains(pl []grid.Point, p grid.Point) bool {
	for _, plp := range pl {
		if plp.Eq(p) {
			return true
		}
	}
	return false
}

func findBasinSize(g *grid.Grid, p grid.Point) int {
	bps := []grid.Point{p}
	checked := []grid.Point{p}
	checkQ := PointQueue{}
	checkQ.Push(p)

	for cp, ok := checkQ.Pop(); ok; cp, ok = checkQ.Pop() {
		for _, op := range grid.OrthogonalOffsets {
			tp := cp.Add(op)
			if !pointListContains(checked, tp) {
				checked = append(checked, tp)
			}

			opv := g.GetPSafe(tp, 9)

			// Add valid point the basin
			if opv < 9 && !pointListContains(bps, tp) {
				bps = append(bps, tp)
				checkQ.Push(tp)
			}
		}
	}
	return len(bps)
}

func day9part2(data string) {
	fmt.Println("Day 9 Part 2")
	g := getGrid(data)
	lp := getLowPoints(&g)
	basinSizes := make([]int, len(lp))
	for i := 0; i < len(lp); i++ {
		basinSizes[i] = findBasinSize(&g, lp[i])
	}
	sort.Ints(basinSizes)
	ans := basinSizes[len(basinSizes)-1] * basinSizes[len(basinSizes)-2] * basinSizes[len(basinSizes)-3]
	fmt.Println("Answer:", ans)
}
