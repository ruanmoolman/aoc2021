package main

import (
	"aoc2021/grid"
	"fmt"
	"image"
	"strconv"
	"strings"
)

func gridFromString(in string) grid.Grid {
	ls := strings.Split(in, "\n")
	g := grid.New(len(ls[0]), len(ls))
	g.SetSeparator("")

	for y, l := range ls {
		for x, v := range l {
			i, err := strconv.Atoi(string(v))
			if err != nil {
				panic(err)
			}
			g.Set(x, y, i)
		}
	}
	return g
}
func Explode(g *grid.Grid) int {
	for _, p := range g.Points() {
		g.AddP(p, 1)
	}

	totalExplosions := 0
	e := grid.New(g.Width(), g.Height())
	for {
		explosions := 0
		for _, p := range g.Points() {
			if g.GetP(p) > 9 && e.GetP(p) == 0 {
				e.SetP(p, 1)
				explosions++
				for xo := -1; xo <= 1; xo++ {
					for yo := -1; yo <= 1; yo++ {
						g.AddP(p.Add(grid.NewPoint(xo, yo)), 1)
					}
				}
			}
		}
		totalExplosions += explosions
		if explosions == 0 {
			break
		}
	}

	for _, p := range g.Points() {
		if g.GetP(p) > 9 {
			g.SetP(p, 0)
		}
	}
	return totalExplosions
}

func day11part1(in string) {
	fmt.Println("Day 11 Part 1")
	totalExplosions := 0

	g := gridFromString(in)

	for s := 1; s <= 100; s++ {
		explosions := Explode(&g)
		totalExplosions += explosions
		fmt.Println(g)
	}
	fmt.Println("Answer:", totalExplosions)
	fmt.Println("Want: 1656")
}

func day11part2(in string) {
	_day11part2(in, false)
}
func day11gif(in string) {
	_day11part2(in, true)
}

func _day11part2(in string, outputGif bool) {
	fmt.Println("Day 11 Part 2")
	imgs := make([]*image.Paletted, 0)
	g := gridFromString(in)

	for s := 1; true; s++ {
		totalExplosions := Explode(&g)

		if outputGif {
			imgs = append(imgs, g.ToPaletted(20))
		}

		if totalExplosions != g.Width()*g.Height() {
			continue
		}
		fmt.Println("Answer:", s)
		break
	}
	if outputGif {
		err := grid.SaveAsGif("output/11/light.gif", imgs)
		if err != nil {
			fmt.Println("Failed to write gif:", err.Error())
		}
	}

	fmt.Println("Want: 195")
}
