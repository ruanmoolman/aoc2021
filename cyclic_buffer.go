package main

import (
	"fmt"
	"strings"
)

type CircularBuffer struct {
	n int
	i int
	b []int
}

func NewCircularBuffer(n int) CircularBuffer {
	return CircularBuffer{n: n, b: make([]int, n), i: 0}
}

func (b *CircularBuffer) String() string {
	ss := make([]string, b.n)
	for i := 0; i < b.n; i++ {
		ss[i] = fmt.Sprint(b.Get(i))
	}
	return strings.Join(ss, ", ")
}

func (b *CircularBuffer) Set(i, v int) {
	b.b[modN(b.i+i, b.n)] = v
}

func (b *CircularBuffer) Get(i int) int {
	return b.b[modN(b.i+i, b.n)]
}

func (b *CircularBuffer) Step() {
	b.i++
}

func (b *CircularBuffer) Sum() int {
	s := 0
	for i := 0; i < b.n; i++ {
		s = s + b.Get(i)
	}
	return s
}
