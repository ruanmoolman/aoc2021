package main

type Node struct {
	name string  // should be unique
	cons []*Node // outgoing connections
}

func (n *Node) String() string {
	s := "(" + n.name + " > "
	for _, c := range n.cons {
		s = s + c.name + " "
	}
	return s[:len(s)-1] + ")"
}

func (n *Node) Eq(on *Node) bool {
	return on == nil || n.name == on.name
}

func (n *Node) Connect(on *Node) {
	// Don't add existing connection
	if FindNodeByName(n.cons, on.name) != nil {
		return
	}
	n.cons = append(n.cons, on)
}

func FindNodeByName(ns []*Node, name string) *Node {
	for _, n := range ns {
		if n.name == name {
			return n
		}
	}
	return nil
}
