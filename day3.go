package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

func gammaEpsilon(d string) (int64, int64) {
	ls := strings.Split(d, "\n")
	N := len(ls)
	n := len(ls[0])
	ones := make([]int, n)
	for _, l := range ls {
		for i, c := range l {
			if c == '1' {
				ones[i]++
			}
		}
	}
	gam := ""
	eps := ""
	for i := 0; i < n; i++ {
		if ones[i] > N-ones[i] {
			gam = gam + "1"
			eps = eps + "0"
		} else {
			gam = gam + "0"
			eps = eps + "1"
		}
	}

	g, _ := strconv.ParseInt(gam, 2, 0)
	e, _ := strconv.ParseInt(eps, 2, 0)
	return g, e
}

func day3part1(data string) {
	fmt.Println("Day 3 Part 1")
	g, e := gammaEpsilon(data)
	fmt.Println("Gamma:", g)
	fmt.Println("Epsilon:", e)
	fmt.Println("Answer:", g*e)
}

func countCharAtIdx(ls []string, b byte, idx int) (int, error) {
	if len(ls) <= 0 {
		return 0, errors.New("no lines to count")
	}
	n := len(ls[0])
	if idx < 0 || idx > n {
		return 0, errors.New("out of bounds")
	}
	c := 0
	for _, l := range ls {
		if l[idx] == b {
			c++
		}
	}
	return c, nil
}

func filterByIndexValue(ls []string, idx int, val rune) ([]string, error) {
	nl := make([]string, 0, len(ls))
	for _, v := range ls {
		if []rune(v)[idx] == val {
			nl = append(nl, v)
		}
	}
	return nl, nil
}

func filterList(ls []string, most bool) (string, error) {
	var ones int
	var err error
	i := 0
	for {
		N := len(ls)
		ones, err = countCharAtIdx(ls, '1', i)
		if err != nil {
			fmt.Println("Error counting index:", err.Error())
			return "", err
		}
		fmt.Printf("1 vs. 0 [%v]: %v/%v\n", i, ones, N-ones)
		if most {
			if ones >= N-ones {
				// One should stay
				ls, err = filterByIndexValue(ls, i, '1')
			} else {
				// Zero should stay
				ls, err = filterByIndexValue(ls, i, '0')
			}
		} else {
			if ones < N-ones {
				// One should stay
				ls, err = filterByIndexValue(ls, i, '1')
			} else {
				// Zero should stay
				ls, err = filterByIndexValue(ls, i, '0')
			}
		}
		if err != nil {
			fmt.Println("Error filtering:", err.Error())
			return "", err
		}
		fmt.Println("L:", ls)
		if len(ls) == 1 {
			return ls[0], nil
		}
		i++
	}
}

func o2co2(d string) (int64, int64) {
	ls := strings.Split(d, "\n")
	fmt.Println("L:", ls)
	oxl, err := filterList(ls, true)
	if err != nil {
		fmt.Println("Error filtering ox:", err.Error())
		return 0, 0
	}
	fmt.Println("Oxygen gen", oxl)
	co2l, err := filterList(ls, false)
	if err != nil {
		fmt.Println("Error filtering ox:", err.Error())
		return 0, 0
	}
	fmt.Println("CO2 gen", co2l)

	o2c, _ := strconv.ParseInt(oxl, 2, 0)
	co2c, _ := strconv.ParseInt(co2l, 2, 0)
	return o2c, co2c
}

func day3part2(data string) {
	fmt.Println("Day 3 Part 2")
	o2, co2 := o2co2(data)
	fmt.Println("O2:", o2)
	fmt.Println("Co2:", co2)
	fmt.Println("Answer:", o2*co2)
}
