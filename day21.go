package main

import (
	"fmt"
	"math"
)

const NumPlayers = 2
const NumTurnRolls = 3
const RouteLength = 10

var WinScore = 21

type DiracDiceGame struct {
	gameCount    int
	activePlayer int
	turnCount    int
	turnRolls    int
	diceRolls    int
	scores       [NumPlayers]int
	positions    [NumPlayers]int
}

func (g *DiracDiceGame) Copy() *DiracDiceGame {
	return &DiracDiceGame{
		gameCount:    g.gameCount,
		activePlayer: g.activePlayer,
		diceRolls:    g.diceRolls,
		scores:       [2]int{g.scores[0], g.scores[1]},
		positions:    [2]int{g.positions[0], g.positions[1]},
	}
}

func (g *DiracDiceGame) String() string {
	return fmt.Sprintf("Dirac Dice Game State:\n  Scores:%v\n  Positions:%v\n  #Rolls:%v",
		g.scores, g.Positions(), g.NumRolls())
}

func NewDiracDiceGame(playerPositions [NumPlayers]int) *DiracDiceGame {
	pos := [NumPlayers]int{}
	for i, pp := range playerPositions {
		pos[i] = pp - 1
	}
	return &DiracDiceGame{
		gameCount: 1,
		positions: pos,
		scores:    [NumPlayers]int{},
	}
}
func (g *DiracDiceGame) Roll(diceValue int) {
	g.turnCount = g.turnCount + diceValue
	g.turnRolls++
	if g.turnRolls == NumTurnRolls {
		g.Turn(g.turnCount, 1)
		g.turnCount = 0
		g.turnRolls = 0
	}
}
func (g *DiracDiceGame) Turn(moveCount, numGames int) {
	g.gameCount = g.gameCount * numGames
	g.diceRolls = g.diceRolls + NumTurnRolls
	// if the dice is rolled three times, score for that player
	g.positions[g.activePlayer] = (g.positions[g.activePlayer] + moveCount) % RouteLength
	g.scores[g.activePlayer] = g.scores[g.activePlayer] + g.positions[g.activePlayer] + 1
	g.activePlayer = (g.activePlayer + 1) % len(g.positions)
}
func (g *DiracDiceGame) NumRolls() int {
	return g.diceRolls
}
func (g *DiracDiceGame) Finished() bool {
	for _, v := range g.scores {
		if v >= WinScore {
			return true
		}
	}
	return false
}
func (g *DiracDiceGame) Leader() int {
	if g.scores[0] > g.scores[1] {
		return 0
	} else {
		return 1
	}
}
func (g *DiracDiceGame) Scores() [NumPlayers]int {
	return g.scores
}
func (g *DiracDiceGame) Positions() [NumPlayers]int {
	pos := [NumPlayers]int{}
	for i, pp := range g.positions {
		pos[i] = pp + 1
	}
	return pos
}

func day21part1(data string) {
	fmt.Println("Day 21 Part 1")
	WinScore = 1000
	//test
	//game := NewDiracDiceGame(10, []int{4, 8})
	//res := 739785
	//puzzle
	game := NewDiracDiceGame([2]int{10, 4})
	res := 908091
	diceValue := 0
	for !game.Finished() {
		diceValue++
		if diceValue > 100 {
			diceValue = 1
		}
		game.Roll(diceValue)
	}
	fmt.Println(game)
	losingScore := math.MaxInt64
	for _, s := range game.Scores() {
		if s < losingScore {
			losingScore = s
		}
	}
	fmt.Println("Answer:", losingScore*game.NumRolls())
	fmt.Println("Test:", res)
}

func day21part2(data string) {
	fmt.Println("Day 21 Part 2")
	rolls := map[int]int{}
	for i := 1; i <= 3; i++ {
		for j := 1; j <= 3; j++ {
			for k := 1; k <= 3; k++ {
				rolls[i+j+k]++
			}
		}
	}

	games := make([]*DiracDiceGame, 0, 1000)
	// test
	//games = append(games, NewDiracDiceGame([NumPlayers]int{4, 8}))
	//p1, p2 := 444356092776315, 341960390180808
	// puzzle
	games = append(games, NewDiracDiceGame([NumPlayers]int{10, 4}))
	p1, p2 := 190897246590017, 90710140491134
	wins := [NumPlayers]int{}
	for len(games) > 0 {
		game := games[0]
		games = games[1:]
		for rollsValues, rollsCount := range rolls {
			newGame := game.Copy()
			newGame.Turn(rollsValues, rollsCount)
			if newGame.Finished() {
				wins[newGame.Leader()] = wins[newGame.Leader()] + newGame.gameCount
			} else {
				games = append(games, newGame)
			}
		}
	}
	winner := 1
	if wins[1] > wins[0] {
		winner = 2
	}
	fmt.Println("Wins", wins, winner)
	fmt.Printf("Test: P1:%v P2:%v\n", p1, p2)
}
