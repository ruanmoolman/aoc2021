package main_test

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestDay20(t *testing.T) {
	cases := []struct {
		name string
		in   string
		want string
	}{
		{name: "simple pair",
			in:   "asdf",
			want: "asdf",
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			require.Equal(t, c.in, c.want)
		})
	}
}
