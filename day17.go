package main

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
)

type Area struct {
	xMin, xMax, yMin, yMax int
}

func MustAtoi(s string) int {
	v, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return v
}

func parseArea(s string) Area {
	xrRe := regexp.MustCompile(`[xy]=(-?\d+)\.\.(-?\d+)`)
	m := xrRe.FindAllStringSubmatch(s, -1)
	return Area{
		xMin: MustAtoi(m[0][1]),
		xMax: MustAtoi(m[0][2]),
		yMin: MustAtoi(m[1][1]),
		yMax: MustAtoi(m[1][2]),
	}
}

type Shot struct {
	x, y, t int
}

func (v Shot) Equals(o Shot) bool {
	return v.x == o.x && v.y == o.y
}

func maxYVel(y int) int {
	if y < 0 {
		return -y - 1
	} else if y > 0 {
		return y
	} else {
		return math.MaxInt64
	}
}

func distOverTime(s, t int) int {
	// Speed diminishes 1 per tick
	bt := dimSum(s)           // distance until 0 speeds (big triangle)
	st := dimSum(max(s-t, 0)) // distance travelled after ticks=t (small triangle)
	return bt - st
}

func day17part1(data string) {
	fmt.Println("Day 17 Part 1")
	a := parseArea(data)
	v := maxYVel(a.yMin)
	fmt.Println(v)
	fmt.Println("Answer: ", dimSum(v))
	fmt.Println("Test: 45")
	fmt.Println("Main: 5460")
}

// possibleShots returns a list of shots that would hit the target y value
// after some number of ticks. y must be a negative number.
func possibleShots(y int) []Shot {
	maxYV := maxYVel(y)

	ys := make([]Shot, 0)
	// NOTE: assuming y is always negative
	for yv := maxYV; yv >= y; yv-- {
		s := 0
		steps := 0
		for d := yv; ; d-- {
			steps++
			s = s + d
			if s < y {
				break
			}
			if s == y {
				ys = append(ys, Shot{y: yv, t: steps})
				break
			}
		}
	}
	return ys
}

// initialVelocity calculates the velocity required to hit the target value x after t ticks
// if the velocity decreases by 1 every tick. x must be a positive value and the function
// returns 0 if the target could not be hit.
func initialVelocity(x int, t int) int {
	for v := 1; v <= x; v++ {
		d := distOverTime(v, t)
		if d == x {
			return v
		}
	}
	return 0
}

func shotsOnTarget(x, y int) []Shot {
	shots := make([]Shot, 0)
	for _, shot := range possibleShots(y) {
		xv := initialVelocity(x, shot.t)
		if xv > 0 {
			shots = append(shots, Shot{xv, shot.y, shot.t})
		}
	}
	return shots
}

func shotsOnArea(a Area) []Shot {
	shots := make([]Shot, 0)
	for x := a.xMin; x <= a.xMax; x++ {
		for y := a.yMin; y <= a.yMax; y++ {
			shots = append(shots, shotsOnTarget(x, y)...)
		}
	}
	return uniqueShots(shots)
}

func uniqueShots(shots []Shot) []Shot {
	uShots := make([]Shot, 0)
shotLoop:
	for _, s := range shots {
		for _, u := range uShots {
			if s.Equals(u) {
				continue shotLoop
			}
		}
		uShots = append(uShots, s)
	}
	return uShots
}

func day17part2(data string) {
	fmt.Println("Day 17 Part 2")
	a := parseArea(data)
	vs := shotsOnArea(a)
	fmt.Println("Answer:", len(vs))
	fmt.Println("Test: 112")
	fmt.Println("Main: 3618")
}
