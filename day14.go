package main

import (
	"fmt"
	"math"
	"strings"
)

func growPolymer(start, rules string, steps int) string {
	start = strings.TrimSpace(start)
	rules = strings.TrimSpace(rules)
	rs := map[string]string{}
	for _, r := range strings.Split(rules, "\n") {
		ruleRes := strings.Split(r, " -> ")
		rs[ruleRes[0]] = ruleRes[1]
	}

	poly := start
	newPoly := ""
	for i := 0; i < steps; i++ {
		for ci := 0; ci < len(poly); ci++ {
			newPoly = newPoly + string(poly[ci])
			if ci < len(poly)-1 {
				ruleOutput, ok := rs[poly[ci:ci+2]]
				if ok {
					newPoly = newPoly + ruleOutput
				}
			}
		}
		poly = newPoly
		newPoly = ""
	}
	return poly
}

func scorePolymer(pol string) int {
	count := map[rune]int{}
	for _, v := range pol {
		count[v]++
	}
	maxVal := 0
	minVal := math.MaxInt64
	for _, v := range count {
		if v > maxVal {
			maxVal = v
		}
		if v < minVal {
			minVal = v
		}
	}
	return maxVal - minVal
}

func day14part1(data string) {
	fmt.Println("Day 14 Part 1")
	parts := strings.Split(data, "\n\n")
	polymer := growPolymer(parts[0], parts[1], 10)
	fmt.Println("Answer:", scorePolymer(polymer))
	fmt.Println("Want: 1588")
}

type RuleExpandMap = map[string][]string
type RuleCount = map[string]int

func extractRules(ruleData string) RuleExpandMap {
	ruleStrings := strings.Split(ruleData, "\n")
	numRules := len(ruleStrings)

	ruleMap := RuleExpandMap{}
	for i := 0; i < numRules; i++ {
		ruleParts := strings.Split(ruleStrings[i], " -> ")
		in := ruleParts[0]
		out := ruleParts[1]
		ruleMap[in] = []string{in[:1] + out, out + in[1:]}

	}
	return ruleMap
}

func initRuleCount(poly string) RuleCount {
	rc := RuleCount{}
	for i := 0; i < len(poly)-1; i++ {
		rule := poly[i : i+2]
		rc[rule]++
	}
	return rc
}

func expandRules(ruleCount RuleCount, rules RuleExpandMap) RuleCount {
	newRC := RuleCount{}
	for rule, rc := range ruleCount {
		for _, nextRule := range rules[rule] {
			newRC[nextRule] += rc
		}
	}
	return newRC
}

func calcRuleScore(ruleCount RuleCount, trailingChar byte) int {
	charCount := map[byte]int{}
	charCount[trailingChar]++
	for rule, rc := range ruleCount {
		letter := rule[0]
		charCount[letter] = charCount[letter] + rc
	}
	maxVal := 0
	minVal := math.MaxInt64
	for _, v := range charCount {
		if v > maxVal {
			maxVal = v
		}
		if v < minVal {
			minVal = v
		}
	}
	return maxVal - minVal
}

func _day14part2(data string, steps int) int {
	parts := strings.Split(data, "\n\n")
	poly := parts[0]
	ruleMap := extractRules(parts[1])
	ruleCount := initRuleCount(poly)
	for i := 1; i <= steps; i++ {
		ruleCount = expandRules(ruleCount, ruleMap)
	}
	return calcRuleScore(ruleCount, poly[len(poly)-1])
}
func day14part2(data string) {
	fmt.Println("Day 14 Part 2")
	fmt.Println("Answer:", _day14part2(data, 40))
}
