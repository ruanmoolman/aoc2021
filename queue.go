package main

import "aoc2021/grid"

type PointQueue struct {
	d []grid.Point
}

func (q *PointQueue) Len() int {
	return len(q.d)
}

func (q *PointQueue) Push(p grid.Point) {
	q.d = append(q.d, p)
}

func (q *PointQueue) Pop() (grid.Point, bool) {
	if len(q.d) == 0 {
		return grid.Point{}, false
	}
	p := q.d[0]
	q.d = q.d[1:]
	return p, true
}
