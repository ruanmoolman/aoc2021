package main

import (
	"fmt"
	"testing"
)

type lerpTestCase struct {
	name       string
	a, b, r, o float64
}

func TestLerp(t *testing.T) {
	testCases := []lerpTestCase{
		{"pos start", 1, 2, 0, 1},
		{"pos mid", 1, 2, 0.3, 1.3},
		{"pos end", 1, 2, 1, 2},
		{"neg start", 2, 1, 0, 2},
		{"neg mid", 2, 1, 0.3, 1.7},
		{"neg end", 2, 1, 1, 1},
		{"same start", 2, 2, 0, 2},
		{"same mid", 2, 2, 0.5, 2},
		{"same end", 2, 2, 1, 2},
	}
	for _, tc := range testCases {
		out := lerp(tc.a, tc.b, tc.r)
		if tc.o != out {
			fmt.Printf("Expected lerp(%v,%v,%v)=%v, got %v", tc.a, tc.b, tc.r, tc.o, out)
		}
	}
}
