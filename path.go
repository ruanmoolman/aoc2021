package main

import "strings"

type StringList []string

func (sl *StringList) String() string {
	return strings.Join(*sl, ",")
}

func NewStringList(s ...string) *StringList {
	nsl := StringList(s)
	return &nsl
}

func (sl *StringList) Copy() *StringList {
	np := &StringList{}
	for _, n := range *sl {
		np.Append(n)
	}
	return np
}

func (sl *StringList) Idx(i int) string {
	if i < 0 || i >= len(*sl) {
		return ""
	}
	return []string(*sl)[i]
}

func (sl *StringList) Append(n string) {
	*sl = append(*sl, n)
}

func (sl *StringList) Last() string {
	return sl.Idx(len(*sl) - 1)
}

func (sl *StringList) Contains(s string) bool {
	for _, lv := range *sl {
		if lv == s {
			return true
		}
	}
	return false
}

func (sl *StringList) ContainsCount(s string) int {
	c := 0
	for _, lv := range *sl {
		if lv == s {
			c++
		}
	}
	return c
}
