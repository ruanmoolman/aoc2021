package main_test

import (
	aoc "aoc2021"
	"aoc2021/matrix"
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

var offset0 = matrix.NewMatrixData([]int{0, 0, 0})
var offset1 = matrix.NewMatrixData([]int{68, -1246, -43})
var offset2 = matrix.NewMatrixData([]int{1105, -1205, 1229})
var offset3 = matrix.NewMatrixData([]int{-92, -2380, -20})
var offset4 = matrix.NewMatrixData([]int{-20, -1133, 1061})
var truePoints = matrix.NewMatrixData(
	[]int{-892, 524, 684},
	[]int{-876, 649, 763},
	[]int{-838, 591, 734},
	[]int{-789, 900, -551},
	[]int{-739, -1745, 668},
	[]int{-706, -3180, -659},
	[]int{-697, -3072, -689},
	[]int{-689, 845, -530},
	[]int{-687, -1600, 576},
	[]int{-661, -816, -575},
	[]int{-654, -3158, -753},
	[]int{-635, -1737, 486},
	[]int{-631, -672, 1502},
	[]int{-624, -1620, 1868},
	[]int{-620, -3212, 371},
	[]int{-618, -824, -621},
	[]int{-612, -1695, 1788},
	[]int{-601, -1648, -643},
	[]int{-584, 868, -557},
	[]int{-537, -823, -458},
	[]int{-532, -1715, 1894},
	[]int{-518, -1681, -600},
	[]int{-499, -1607, -770},
	[]int{-485, -357, 347},
	[]int{-470, -3283, 303},
	[]int{-456, -621, 1527},
	[]int{-447, -329, 318},
	[]int{-430, -3130, 366},
	[]int{-413, -627, 1469},
	[]int{-345, -311, 381},
	[]int{-36, -1284, 1171},
	[]int{-27, -1108, -65},
	[]int{7, -33, -71},
	[]int{12, -2351, -103},
	[]int{26, -1119, 1091},
	[]int{346, -2985, 342},
	[]int{366, -3059, 397},
	[]int{377, -2827, 367},
	[]int{390, -675, -793},
	[]int{396, -1931, -563},
	[]int{404, -588, -901},
	[]int{408, -1815, 803},
	[]int{423, -701, 434},
	[]int{432, -2009, 850},
	[]int{443, 580, 662},
	[]int{455, 729, 728},
	[]int{456, -540, 1869},
	[]int{459, -707, 401},
	[]int{465, -695, 1988},
	[]int{474, 580, 667},
	[]int{496, -1584, 1900},
	[]int{497, -1838, -617},
	[]int{527, -524, 1933},
	[]int{528, -643, 409},
	[]int{534, -1912, 768},
	[]int{544, -627, -890},
	[]int{553, 345, -567},
	[]int{564, 392, -477},
	[]int{568, -2007, -577},
	[]int{605, -1665, 1952},
	[]int{612, -1593, 1893},
	[]int{630, 319, -379},
	[]int{686, -3108, -505},
	[]int{776, -3184, -501},
	[]int{846, -3110, -434},
	[]int{1135, -1161, 1235},
	[]int{1243, -1093, 1063},
	[]int{1660, -552, 429},
	[]int{1693, -557, 386},
	[]int{1735, -437, 1738},
	[]int{1749, -1800, 1813},
	[]int{1772, -405, 1572},
	[]int{1776, -675, 371},
	[]int{1779, -442, 1789},
	[]int{1780, -1548, 337},
	[]int{1786, -1538, 337},
	[]int{1847, -1591, 415},
	[]int{1889, -1729, 1762},
	[]int{1994, -1805, 1792},
)
var scanner0 = matrix.NewMatrixData(
	[]int{404, -588, -901},
	[]int{528, -643, 409},
	[]int{-838, 591, 734},
	[]int{390, -675, -793},
	[]int{-537, -823, -458},
	[]int{-485, -357, 347},
	[]int{-345, -311, 381},
	[]int{-661, -816, -575},
	[]int{-876, 649, 763},
	[]int{-618, -824, -621},
	[]int{553, 345, -567},
	[]int{474, 580, 667},
	[]int{-447, -329, 318},
	[]int{-584, 868, -557},
	[]int{544, -627, -890},
	[]int{564, 392, -477},
	[]int{455, 729, 728},
	[]int{-892, 524, 684},
	[]int{-689, 845, -530},
	[]int{423, -701, 434},
	[]int{7, -33, -71},
	[]int{630, 319, -379},
	[]int{443, 580, 662},
	[]int{-789, 900, -551},
	[]int{459, -707, 401},
)
var scanner1 = matrix.NewMatrixData(
	[]int{686, 422, 578},
	[]int{605, 423, 415},
	[]int{515, 917, -361},
	[]int{-336, 658, 858},
	[]int{95, 138, 22},
	[]int{-476, 619, 847},
	[]int{-340, -569, -846},
	[]int{567, -361, 727},
	[]int{-460, 603, -452},
	[]int{669, -402, 600},
	[]int{729, 430, 532},
	[]int{-500, -761, 534},
	[]int{-322, 571, 750},
	[]int{-466, -666, -811},
	[]int{-429, -592, 574},
	[]int{-355, 545, -477},
	[]int{703, -491, -529},
	[]int{-328, -685, 520},
	[]int{413, 935, -424},
	[]int{-391, 539, -444},
	[]int{586, -435, 557},
	[]int{-364, -763, -893},
	[]int{807, -499, -711},
	[]int{755, -354, -619},
	[]int{553, 889, -390},
)
var scanner2 = matrix.NewMatrixData(
	[]int{649, 640, 665},
	[]int{682, -795, 504},
	[]int{-784, 533, -524},
	[]int{-644, 584, -595},
	[]int{-588, -843, 648},
	[]int{-30, 6, 44},
	[]int{-674, 560, 763},
	[]int{500, 723, -460},
	[]int{609, 671, -379},
	[]int{-555, -800, 653},
	[]int{-675, -892, -343},
	[]int{697, -426, -610},
	[]int{578, 704, 681},
	[]int{493, 664, -388},
	[]int{-671, -858, 530},
	[]int{-667, 343, 800},
	[]int{571, -461, -707},
	[]int{-138, -166, 112},
	[]int{-889, 563, -600},
	[]int{646, -828, 498},
	[]int{640, 759, 510},
	[]int{-630, 509, 768},
	[]int{-681, -892, -333},
	[]int{673, -379, -804},
	[]int{-742, -814, -386},
	[]int{577, -820, 562},
)
var scanner3 = matrix.NewMatrixData(
	[]int{-589, 542, 597},
	[]int{605, -692, 669},
	[]int{-500, 565, -823},
	[]int{-660, 373, 557},
	[]int{-458, -679, -417},
	[]int{-488, 449, 543},
	[]int{-626, 468, -788},
	[]int{338, -750, -386},
	[]int{528, -832, -391},
	[]int{562, -778, 733},
	[]int{-938, -730, 414},
	[]int{543, 643, -506},
	[]int{-524, 371, -870},
	[]int{407, 773, 750},
	[]int{-104, 29, 83},
	[]int{378, -903, -323},
	[]int{-778, -728, 485},
	[]int{426, 699, 580},
	[]int{-438, -605, -362},
	[]int{-469, -447, -387},
	[]int{509, 732, 623},
	[]int{647, 635, -688},
	[]int{-868, -804, 481},
	[]int{614, -800, 639},
	[]int{595, 780, -596},
)
var scanner4 = matrix.NewMatrixData(
	[]int{727, 592, 562},
	[]int{-293, -554, 779},
	[]int{441, 611, -461},
	[]int{-714, 465, -776},
	[]int{-743, 427, -804},
	[]int{-660, -479, -426},
	[]int{832, -632, 460},
	[]int{927, -485, -438},
	[]int{408, 393, -506},
	[]int{466, 436, -512},
	[]int{110, 16, 151},
	[]int{-258, -428, 682},
	[]int{-393, 719, 612},
	[]int{-211, -452, 876},
	[]int{808, -476, -593},
	[]int{-575, 615, 604},
	[]int{-485, 667, 467},
	[]int{-680, 325, -822},
	[]int{-627, -443, -432},
	[]int{872, -547, -609},
	[]int{833, 512, 582},
	[]int{807, 604, 487},
	[]int{839, -516, 451},
	[]int{891, -625, 532},
	[]int{-652, -548, -490},
	[]int{30, -46, -14},
)

func TestRotateX(t *testing.T) {
	in := aoc.NewPoint3D(1, 2, 4)
	cases := []struct {
		steps int
		want  *matrix.Matrix
	}{
		{0, aoc.NewPoint3D(1, 2, 4)},
		{1, aoc.NewPoint3D(1, 4, -2)},
		{2, aoc.NewPoint3D(1, -2, -4)},
		{3, aoc.NewPoint3D(1, -4, 2)},
		{4, aoc.NewPoint3D(1, 2, 4)},
		{-1, aoc.NewPoint3D(1, -4, 2)},
		{-2, aoc.NewPoint3D(1, -2, -4)},
		{-3, aoc.NewPoint3D(1, 4, -2)},
	}
	for _, c := range cases {
		t.Run(fmt.Sprintf("Rotate %v about X, %v times", in, c.steps),
			func(t *testing.T) {
				got := aoc.RotateX(in, c.steps)
				require.Equal(t, c.want, got)
			})
	}
}

func TestRotateY(t *testing.T) {
	in := aoc.NewPoint3D(1, 2, 3)
	cases := []struct {
		steps int
		want  *matrix.Matrix
	}{
		{0, aoc.NewPoint3D(1, 2, 3)},
		{1, aoc.NewPoint3D(-3, 2, 1)},
		{2, aoc.NewPoint3D(-1, 2, -3)},
		{3, aoc.NewPoint3D(3, 2, -1)},
		{4, aoc.NewPoint3D(1, 2, 3)},
		{-1, aoc.NewPoint3D(3, 2, -1)},
		{-2, aoc.NewPoint3D(-1, 2, -3)},
		{-3, aoc.NewPoint3D(-3, 2, 1)},
	}
	for _, c := range cases {
		t.Run(fmt.Sprintf("Rotate %v about Y, %v times", in, c.steps),
			func(t *testing.T) {
				got := aoc.RotateY(in, c.steps)
				require.Equal(t, c.want, got)
			})
	}
}

func TestRotateZ(t *testing.T) {
	in := aoc.NewPoint3D(1, 2, 3)
	cases := []struct {
		steps int
		want  *matrix.Matrix
	}{
		{1, aoc.NewPoint3D(2, -1, 3)},
		{2, aoc.NewPoint3D(-1, -2, 3)},
		{3, aoc.NewPoint3D(-2, 1, 3)},
		{4, aoc.NewPoint3D(1, 2, 3)},
		{-3, aoc.NewPoint3D(2, -1, 3)},
		{-2, aoc.NewPoint3D(-1, -2, 3)},
		{-1, aoc.NewPoint3D(-2, 1, 3)},
		{0, aoc.NewPoint3D(1, 2, 3)},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Rotate %v about Z, %v times", in, c.steps),
			func(t *testing.T) {
				got := aoc.RotateZ(in, c.steps)
				require.Equal(t, c.want, got)
			})
	}
}

func TestRepositionForOrigin(t *testing.T) {
	in := matrix.NewMatrixData(
		[]int{1, 2, 3},
		[]int{-3, 1, 5},
		[]int{2, -4, 2})

	want := []*matrix.Matrix{
		matrix.NewMatrixData(
			[]int{0, 0, 0},
			[]int{-4, -1, 2},
			[]int{1, -6, -1}),
		matrix.NewMatrixData(
			[]int{4, 1, -2},
			[]int{0, 0, 0},
			[]int{5, -5, -3}),
		matrix.NewMatrixData(
			[]int{-1, 6, 1},
			[]int{-5, 5, 3},
			[]int{0, 0, 0}),
	}
	for r := 0; r < in.Rows(); r++ {
		t.Run(fmt.Sprintf("Reposition on %v", in.GetRow(r)),
			func(t *testing.T) {
				got := aoc.RepositionForOrigin(in, in.GetRow(r))
				require.Equal(t, want[r], got)
			})
	}
}

func TestCountMatchingRows(t *testing.T) {
	cases := []struct {
		a, b *matrix.Matrix
		want int
	}{
		{
			matrix.NewMatrixData(
				[]int{1, 2, 3},
				[]int{-3, 1, 5},
				[]int{2, -4, 2}),
			matrix.NewMatrixData(
				[]int{1, 2, 3},
				[]int{-3, 1, 5},
				[]int{2, -4, 2}),
			3,
		},
		{
			matrix.NewMatrixData(
				[]int{-30, 1, 11},
				[]int{9, -11, 15},
				[]int{5, 17, -34},
				[]int{-7, 2, 8}),
			matrix.NewMatrixData(
				[]int{5, 17, -34},
				[]int{-30, 1, 11},
				[]int{4, -12, 27},
				[]int{9, -11, 15}),
			3,
		},
	}

	for i, c := range cases {
		t.Run(fmt.Sprintf("CountMatching %v", i),
			func(t *testing.T) {
				got := aoc.CountMatchingRows(c.a, c.b)
				require.Equal(t, c.want, got)
			})
	}
}

func TestPointOverlap(t *testing.T) {
	cases := []struct {
		name       string
		a, b       *matrix.Matrix
		wantOffset *matrix.Matrix
		wantCount  int
	}{
		{
			"empty lists",
			matrix.NewMatrixData(
				[]int{1, 2, 3},
				[]int{-3, -2, -1}),
			matrix.NewMatrixData(
				[]int{1, 2, 3},
				[]int{-3, -2, -1}),
			aoc.NewPoint3D(0, 0, 0),
			2,
		},
		{
			"same lists, with one point not matching ",
			matrix.NewMatrixData(
				[]int{1, 2, 5},
				[]int{6, -3, 0}),
			matrix.NewMatrixData(
				[]int{6, -3, 0},
				[]int{4, -10, 2},
				[]int{1, 2, 5}),
			aoc.NewPoint3D(0, 0, 0),
			2,
		},
		{
			"same lists, with one point not matching ",
			matrix.NewMatrixData(
				[]int{1, 2, 5},
				[]int{6, -3, 0}),
			matrix.NewMatrixData(
				[]int{2, -1, 11},
				[]int{0, -8, 13},
				[]int{-3, 4, 16}),
			aoc.NewPoint3D(4, -2, -11),
			2,
		},
		{
			"same lists, with first not matching",
			matrix.NewMatrixData(
				[]int{0, 19, 2},
				[]int{1, 3, 4},
				[]int{2, -2, 2}),
			matrix.NewMatrixData(
				[]int{-7, 0, 8},
				[]int{-6, -5, 6}),
			aoc.NewPoint3D(8, 3, -4),
			2,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			gotOffset, gotCount := aoc.PointOverlap(c.a, c.b)
			require.Equal(t, c.wantOffset, gotOffset)
			require.Equal(t, c.wantCount, gotCount)
		})
	}
}

func TestScannerOverlap(t *testing.T) {
	cases := []struct {
		points       []*matrix.Matrix
		wantPosition *matrix.Matrix
		wantOverlaps bool
	}{
		{[]*matrix.Matrix{truePoints, scanner0},
			offset0, true},
		{[]*matrix.Matrix{truePoints, scanner1},
			offset1, true},
		{[]*matrix.Matrix{truePoints, scanner2},
			offset2, true},
		{[]*matrix.Matrix{truePoints, scanner3},
			offset3, true},
		{[]*matrix.Matrix{truePoints, scanner4},
			offset4, true},
		{[]*matrix.Matrix{scanner0, scanner1, scanner0},
			offset0, true},
		{[]*matrix.Matrix{scanner0, scanner1, scanner4},
			offset4, true},
		{[]*matrix.Matrix{scanner1, scanner2},
			nil, false},
		{[]*matrix.Matrix{scanner0, scanner1, scanner3},
			offset3, true},
	}

	for i, c := range cases {
		t.Run(fmt.Sprintf("Test %v", i), func(t *testing.T) {
			scanner := &aoc.Scanner{
				Id: 0, Beacons: c.points[0],
				Position: matrix.Vec([]int{0, 0, 0}),
			}

			var compareScanner *aoc.Scanner
			var gotOverlaps bool
			for i := 1; i < len(c.points); i++ {
				compareScanner = &aoc.Scanner{Id: 1, Beacons: c.points[i]}
				gotOverlaps = aoc.ScannerOverlap(scanner, compareScanner)
				scanner = compareScanner
			}

			require.Equal(t, c.wantOverlaps, gotOverlaps)
			require.Equal(t, scanner.Position, c.wantPosition)
		})
	}
}
