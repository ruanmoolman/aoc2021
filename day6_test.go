package main

import (
	"fmt"
	"testing"
)

var testData = "3,4,3,1,2"
var puzzleData = "1,1,5,2,1,1,5,5,3,1,1,1,1,1,1,3,4,5,2,1,2,1,1,1,1,1,1,1,1,3,1,1,5,4,5,1,5,3,1,3,2,1,1,1,1,2,4,1,5,1,1,1,4,4,1,1,1,1,1,1,3,4,5,1,1,2,1,1,5,1,1,4,1,4,4,2,4,4,2,2,1,2,3,1,1,2,5,3,1,1,1,4,1,2,2,1,4,1,1,2,5,1,3,2,5,2,5,1,1,1,5,3,1,3,1,5,3,3,4,1,1,4,4,1,3,3,2,5,5,1,1,1,1,3,1,5,2,1,3,5,1,4,3,1,3,1,1,3,1,1,1,1,1,1,5,1,1,5,5,2,1,5,1,4,1,1,5,1,1,1,5,5,5,1,4,5,1,3,1,2,5,1,1,1,5,1,1,4,1,1,2,3,1,3,4,1,2,1,4,3,1,2,4,1,5,1,1,1,1,1,3,4,1,1,5,1,1,3,1,1,2,1,3,1,2,1,1,3,3,4,5,3,5,1,1,1,1,1,1,1,1,1,5,4,1,5,1,3,1,1,2,5,1,1,4,1,1,4,4,3,1,2,1,2,4,4,4,1,2,1,3,2,4,4,1,1,1,1,4,1,1,1,1,1,4,1,5,4,1,5,4,1,1,2,5,5,1,1,1,5"

func BenchmarkDay6Part1(b *testing.B) {
	for n := 0; n < b.N; n++ {
		run(puzzleData, 80)
	}
}

func BenchmarkDay6Part2(b *testing.B) {
	for n := 0; n < b.N; n++ {
		run(puzzleData, 256)
	}
}

func BenchmarkDay6Part1CB(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runOpt(puzzleData, 80)
	}
}

func BenchmarkDay6Part2CB(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runOpt(puzzleData, 256)
	}
}

type testCase struct {
	in  string
	it  int
	out int
}

func TestDay6Part1CB(t *testing.T) {
	testCases := []testCase{
		{testData, 80, 5934},
		{puzzleData, 80, 372300},
		{testData, 256, 26984457539},
		{puzzleData, 256, 1675781200288},
	}
	for i, tc := range testCases {
		t.Run(fmt.Sprintf("Test #%v", i), func(t *testing.T) {
			r := runOpt(tc.in, tc.it)
			e := tc.out
			if e != r {
				t.Fatalf("Expected %v, got %v", e, r)
			}
		})
	}
}
