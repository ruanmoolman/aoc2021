package main

import (
	"aoc2021/grid"
	"fmt"
	"strings"
)

type SCGrid struct {
	grid  grid.Grid
	ticks int
}

type SCKind = int

const (
	NoneSC  SCKind = 0
	EastSC         = 1
	SouthSC        = 2
)

func NewSCGrid(s string) *SCGrid {
	ls := strings.Split(s, "\n")
	lines := make([]string, 0)
	for _, l := range ls {
		if l == "" {
			continue
		}
		lines = append(lines, l)
	}

	g := grid.New(len(lines[0]), len(lines))
	for y := range lines {
		for x, c := range []byte(lines[y]) {
			switch c {
			case '>':
				g.Set(x, y, EastSC)
			case 'v':
				g.Set(x, y, SouthSC)
			}
		}
	}
	return &SCGrid{grid: g}
}

func (g *SCGrid) String() string {
	lines := make([]string, g.grid.Height())
	for y := 0; y < g.grid.Height(); y++ {
		l := ""
		for x := 0; x < g.grid.Width(); x++ {
			switch g.grid.Get(x, y) {
			case 1:
				l = l + ">"
			case 2:
				l = l + "v"
			default:
				l = l + "."
			}
		}
		lines[y] = l
	}
	return strings.Join(lines, "\n")
}

func (g *SCGrid) east(p grid.Point) grid.Point {
	return grid.NewPoint((p.X()+1)%g.grid.Width(), p.Y())
}
func (g *SCGrid) south(p grid.Point) grid.Point {
	return grid.NewPoint(p.X(), (p.Y()+1)%g.grid.Height())
}

func (g *SCGrid) Tick() int {
	g.ticks++
	// move east
	eastMovers := make([]grid.Point, 0)
	for _, p := range g.grid.Points() {
		if g.grid.GetP(p) == EastSC && g.grid.GetP(g.east(p)) == NoneSC {
			eastMovers = append(eastMovers, p)
		}
	}
	for _, p := range eastMovers {
		g.grid.SetP(p, 0)
		g.grid.SetP(g.east(p), EastSC)
	}
	// move south
	southMovers := make([]grid.Point, 0)
	for _, p := range g.grid.Points() {
		if g.grid.GetP(p) == SouthSC && g.grid.GetP(g.south(p)) == NoneSC {
			southMovers = append(southMovers, p)
		}
	}
	for _, p := range southMovers {
		g.grid.SetP(p, 0)
		g.grid.SetP(g.south(p), SouthSC)
	}
	return len(eastMovers) + len(southMovers)
}
func (g *SCGrid) Ticks() int {
	return g.ticks
}

func day25part1(data string) {
	fmt.Println("Day 25 Part 1")
	scGrid := NewSCGrid(data)
	for scGrid.Tick() != 0 {
	}
	fmt.Println("Answer:", scGrid.Ticks())
}
func day25part2(data string) {
	fmt.Println("Christmas!!")
}
