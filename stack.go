package main

type RuneStack struct {
	d []rune
}

func (r *RuneStack) String() string {
	s := ""
	for _, r := range r.d {
		s = s + string(r)
	}
	return s
}

func (r *RuneStack) Push(v rune) {
	r.d = append(r.d, v)
}

func (r *RuneStack) Pop() rune {
	if len(r.d) == 0 {
		return 0
	}
	v := r.d[len(r.d)-1]
	r.d = r.d[:len(r.d)-1]
	return v
}
