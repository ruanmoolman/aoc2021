package grid

import "fmt"

type Point struct {
	x, y int
}

func NewPoint(x, y int) Point {
	return Point{x, y}
}

func (p Point) X() int {
	return p.x
}
func (p Point) Y() int {
	return p.y
}

func (p Point) Print() string {
	return fmt.Sprintf("(%v %v)", p.x, p.y)
}

func (p Point) Eq(o Point) bool {
	return p.x == o.x && p.y == o.y
}

func (p Point) Add(o Point) Point {
	return Point{p.x + o.x, p.y + o.y}
}

func (p Point) Sub(o Point) Point {
	return Point{p.x - o.x, p.y - o.y}
}

var OrthogonalOffsets = []Point{
	{-1, 0},
	{0, 1},
	{1, 0},
	{0, -1},
}
