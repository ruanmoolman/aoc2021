package grid

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"image/png"
	"math"
	"os"
)

func (g Grid) ToPaletted(scale int) *image.Paletted {
	rect := image.Rect(0, 0, g.w*scale, g.h*scale)
	grayScaleColors := make([]color.Color, math.MaxUint8+1)

	for i := 0; i < math.MaxUint8+1; i++ {
		grayScaleColors[i] = color.Gray{uint8(i)}
	}
	mi := g.max()

	img := image.NewPaletted(rect, grayScaleColors)
	for _, p := range g.Points() {
		v := g.GetP(p)
		pixCol := color.Gray{uint8(float64(v) / float64(mi) * math.MaxUint8)}
		for sx := 0; sx < scale; sx++ {
			for sy := 0; sy < scale; sy++ {
				img.Set(p.x*scale+sx, p.y*scale+sy, pixCol)
			}
		}
	}
	return img
}

func (g Grid) ToImage(scale int) image.Image {
	rect := image.Rect(0, 0, g.w*scale, g.h*scale)
	img := image.NewGray(rect)

	for _, p := range g.Points() {
		v := g.GetP(p)
		wv := uint8(v * math.MaxUint8)
		for sx := 0; sx < scale; sx++ {
			for sy := 0; sy < scale; sy++ {
				img.Set(p.x*scale+sx, p.y*scale+sy, color.Gray{Y: wv})
			}
		}
	}
	return img
}

func SaveAsGif(path string, images []*image.Paletted) error {
	fmt.Printf("Saving gif to %v\n", path)
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	delays := make([]int, len(images))
	for i := range images {
		delays[i] = 1
	}
	delays[len(delays)-1] = 100
	err = gif.EncodeAll(f, &gif.GIF{Image: images, Delay: delays})
	if err != nil {
		return err
	}
	return nil
}

func SaveAsImage(path string, img image.Image) error {
	fmt.Printf("Saving to %v...\n", path)
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	return png.Encode(f, img)
}
