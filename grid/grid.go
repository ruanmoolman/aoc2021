package grid

import (
	"fmt"
	"math"
)

type Grid struct {
	w, h int
	d    [][]int
	pts  []Point
	sep  string
}

func NewSquare(n int) Grid {
	return New(n, n)
}

func New(w, h int) Grid {
	g := Grid{w: w, h: h, d: make([][]int, w), sep: " "}
	for i := 0; i < w; i++ {
		g.d[i] = make([]int, h)
	}
	return g
}

func (g *Grid) SetSeparator(s string) {
	g.sep = s
}

func (g Grid) String() string {
	s := ""
	for y := 0; y < g.Height(); y++ {
		for x := 0; x < g.Width(); x++ {
			s = s + fmt.Sprintf("%v%v", g.Get(x, y), g.sep)
		}
		s = s + "\n"
	}
	return s
}

func (g Grid) max() int {
	m := math.MinInt64
	for _, p := range g.Points() {
		m = max(m, g.GetP(p))
	}
	return m
}

func (g *Grid) Height() int {
	return g.h
}

func (g *Grid) Width() int {
	return g.w
}

func (g *Grid) inBounds(x, y int) bool {
	return x >= 0 && x < g.Width() && y >= 0 && y < g.Height()
}

func (g *Grid) Set(x, y, v int) {
	if !g.inBounds(x, y) {
		return
	}
	g.d[x][y] = v
}

func (g *Grid) SetP(p Point, v int) {
	g.Set(p.x, p.y, v)
}

func (g *Grid) Get(x, y int) int {
	return g.d[x][y]
}

func (g *Grid) GetP(p Point) int {
	return g.Get(p.x, p.y)
}

func (g *Grid) GetSafe(x, y, def int) int {
	if !g.inBounds(x, y) {
		return def
	}
	return g.Get(x, y)
}

func (g *Grid) GetPSafe(p Point, def int) int {
	return g.GetSafe(p.x, p.y, def)
}

func (g *Grid) Points() []Point {
	if g.pts != nil {
		return g.pts
	}
	g.pts = make([]Point, g.Width()*g.Height())
	for y := 0; y < g.Height(); y++ {
		for x := 0; x < g.Width(); x++ {
			g.pts[y*g.Width()+x] = Point{x, y}
		}
	}
	return g.pts
}

func (g *Grid) Add(x, y, v int) {
	g.Set(x, y, g.GetSafe(x, y, 0)+v)
}

func (g *Grid) AddP(p Point, v int) {
	g.Add(p.x, p.y, v)
}
