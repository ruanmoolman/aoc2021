package main

import (
	"fmt"
	"regexp"
	"strings"
)

func buildGraph(conns []string) []*Node {
	nodes := make([]*Node, 0)
	for _, l := range conns {
		parts := strings.Split(l, "-")
		nodeA := FindNodeByName(nodes, parts[0])
		if nodeA == nil {
			nodeA = &Node{name: parts[0]}
		}
		nodeB := FindNodeByName(nodes, parts[1])
		if nodeB == nil {
			nodeB = &Node{name: parts[1]}
		}

		nodeA.Connect(nodeB)
		nodeB.Connect(nodeA)

		if FindNodeByName(nodes, nodeA.name) == nil {
			nodes = append(nodes, nodeA)
		}
		if FindNodeByName(nodes, nodeB.name) == nil {
			nodes = append(nodes, nodeB)
		}
	}
	return nodes
}

var bigCave = regexp.MustCompile("^[A-Z]+$")
var smallCave = regexp.MustCompile("^[a-z]+$")

func isStartCave(name string) bool {
	return name == "start"
}
func isEndCave(name string) bool {
	return name == "end"
}

func isBigCave(name string) bool {
	return bigCave.Match([]byte(name))
}

func isSmallCave(name string) bool {
	return smallCave.Match([]byte(name)) && !isStartCave(name) && !isEndCave(name)
}

func findNumPaths(nodes []*Node) int {
	// Each node is a path
	pathQueue := PathQueue{}
	pathQueue.Push(NewStringList("start"))

	completePaths := make([]*StringList, 0)
	for path, ok := pathQueue.Pop(); ok; path, ok = pathQueue.Pop() {
		node := FindNodeByName(nodes, path.Last())
		for _, c := range node.cons {
			if isBigCave(c.name) || !path.Contains(c.name) {
				newPath := path.Copy()
				newPath.Append(c.name)
				if newPath.Last() == "end" {
					fmt.Println("PATH:", newPath)
					completePaths = append(completePaths, newPath)
				} else {
					pathQueue.Push(newPath)
				}
			}
		}
	}
	return len(completePaths)
}

func calculateNumPaths(data string, fn func([]*Node) int) int {
	lines := strings.Split(data, "\n")
	nodes := buildGraph(lines)
	return fn(nodes)
}

func day12part1(data string) {
	fmt.Println("Day 12 Part 1")
	fmt.Println("Answer:", calculateNumPaths(data, findNumPaths))
	fmt.Println("Want1: 10")
	fmt.Println("Want2: 19")
	fmt.Println("Want3: 226")
}
func visitedSmallTwice(path *StringList) bool {
	smallCaves := make(map[string]bool)
	for _, cave := range *path {
		if !isSmallCave(cave) {
			continue
		}
		if smallCaves[cave] {
			return true
		}
		smallCaves[cave] = true
	}
	return false
}

type Path = StringList
type PathQueue struct {
	d []*Path
}

func (q *PathQueue) Len() int {
	return len(q.d)
}

func (q *PathQueue) Push(p *Path) {
	q.d = append(q.d, p)
}

func (q *PathQueue) Pop() (*Path, bool) {
	if len(q.d) == 0 {
		return nil, false
	}
	p := q.d[0]
	q.d = q.d[1:]
	return p, true
}

func findNumPathsDoubleSmall(nodes []*Node) int {
	// Each node is a path
	pathQueue := PathQueue{}
	pathQueue.Push(NewStringList("start"))

	completePaths := make([]*Path, 0)
	for path, ok := pathQueue.Pop(); ok; path, ok = pathQueue.Pop() {
		node := FindNodeByName(nodes, path.Last())
		for _, c := range node.cons {
			if isBigCave(c.name) ||
				(isSmallCave(c.name) &&
					(!path.Contains(c.name) || !visitedSmallTwice(path))) ||
				isEndCave(c.name) {
				newPath := path.Copy()
				newPath.Append(c.name)
				if isEndCave(c.name) {
					completePaths = append(completePaths, newPath)
				} else {
					pathQueue.Push(newPath)
				}
			}
		}
	}
	return len(completePaths)
}

func day12part2(data string) {
	fmt.Println("Day 12 Part 2")
	fmt.Println("Answer:", calculateNumPaths(data, findNumPathsDoubleSmall))
	fmt.Println("Test:  36")
	fmt.Println("Test2: 103")
	fmt.Println("Test3: 3509")
	fmt.Println("Main:  131228")
}
