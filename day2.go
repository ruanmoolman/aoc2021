package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

const HOR = 1
const VER = 2

func toAction(l string) (int, int) {
	vals := strings.Split(l, " ")
	num, err := strconv.Atoi(vals[1])
	if err != nil {
		panic(err)
	}
	switch vals[0] {
	case "forward":
		return HOR, num
	case "backward":
		return HOR, -num
	case "down":
		return VER, num
	case "up":
		return VER, -num
	default:
		panic(errors.New("invalid action"))
	}
}

func calcDistDepth(ls []string) (int, int) {
	var x, y, dir, val int
	for _, l := range ls {
		dir, val = toAction(l)
		switch dir {
		case HOR:
			x = x + val
		case VER:
			y = y + val
		}
	}
	return x, y
}

func calcWithAim(ls []string) (int, int) {
	var x, y, aim, dir, val int
	for _, l := range ls {
		dir, val = toAction(l)
		switch dir {
		case HOR:
			x = x + val
			y = y + val*aim

		case VER:
			aim = aim + val
		}
	}
	return x, y
}

func day2(data string, fn func([]string) (int, int)) {
	lines := strings.Split(data, "\n")
	lines = filterStrings(lines, func(s string) bool { return s != "" })
	fmt.Println("Data:", lines)
	x, y := fn(lines)
	fmt.Println("Distance:", x)
	fmt.Println("Depth:", y)
	fmt.Println("Answer:", x*y)
}

func day2part1(data string) {
	fmt.Println("Day 2 Task 1")
	day2(data, calcDistDepth)
}

func day2part2(data string) {
	fmt.Println("Day 2 Task 2")
	day2(data, calcWithAim)
}
