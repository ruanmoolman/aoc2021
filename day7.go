package main

import (
	"fmt"
	"math"
	"strings"
)

func part2FuelCost(d int) int {
	return d * (d + 1) / 2
}

func part1FuelCost(d int) int {
	return d
}

func calculateFuel(vs []int, costFn func(int) int) int {
	maxV := 0
	for _, v := range vs {
		maxV = max(maxV, v)
	}
	minV := 0
	for _, v := range vs {
		minV = min(minV, v)
	}
	bestFuel := math.MaxInt64
	for i := minV; i <= maxV; i++ {
		posFuel := 0
		for _, v := range vs {
			posFuel = posFuel + costFn(abs(i-v))
		}
		bestFuel = min(bestFuel, posFuel)
	}
	return bestFuel
}

func day7part1(data string) {
	fmt.Println("Day 7 Part 1")
	vals, err := toInts(strings.Split(data, ","))
	if err != nil {
		panic(err)
	}
	fmt.Println("Answer:", calculateFuel(vals, part1FuelCost))
}

func day7part2(data string) {
	fmt.Println("Day 7 Part 2")
	vals, err := toInts(strings.Split(data, ","))
	if err != nil {
		panic(err)
	}
	fmt.Println("Answer:", calculateFuel(vals, part2FuelCost))
}
