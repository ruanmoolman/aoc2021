package main_test

import (
	"testing"
)
import aoc "aoc2021"

func TestSnailParsing(t *testing.T) {
	cases := []struct {
		name string
		in   string
		want aoc.SE
	}{
		{name: "simple pair",
			in: "[1,1]",
			want: aoc.SE{
				[]aoc.SV{
					{1, 1},
					{1, 1}}},
		},
		{name: "simple multi digit value",
			in: "[15,99]",
			want: aoc.SE{
				[]aoc.SV{
					{15, 1},
					{99, 1}}},
		},
		{name: "simple pair with value",
			in: "[[1,1],2]",
			want: aoc.SE{
				[]aoc.SV{
					{1, 2},
					{1, 2},
					{2, 1}}},
		},
		{name: "nested pair",
			in: "[[1,1],[2,2]]",
			want: aoc.SE{
				[]aoc.SV{
					{1, 2},
					{1, 2},
					{2, 2},
					{2, 2}}},
		},
		{name: "complex",
			in: "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]",
			want: aoc.SE{
				[]aoc.SV{
					{0, 3},
					{4, 4},
					{5, 4},
					{0, 3},
					{0, 3},
					{4, 4},
					{5, 4},
					{2, 4},
					{6, 4},
					{9, 3},
					{5, 3}}},
		},
		{name: "many levels",
			in: "[[[[[9,8],1],2],3],4]",
			want: aoc.SE{
				[]aoc.SV{
					{9, 5},
					{8, 5},
					{1, 4},
					{2, 3},
					{3, 2},
					{4, 1}}},
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := aoc.NewSE(c.in)
			if !c.want.Equal(got) {
				t.Fatalf("Expected %v, got %v", c.want, got)
			}
		})
	}
}

func TestSnailAddition(t *testing.T) {
	cases := []struct {
		name  string
		left  string
		right string
		want  string
	}{
		{name: "add increases depth",
			left:  "[1,1]",
			right: "[2,2]",
			want:  "[[1,1],[2,2]]",
		},
		{name: "add increases depth 2",
			left:  "[[1,1],[2,2]]",
			right: "[3,3]",
			want:  "[[[1,1],[2,2]],[3,3]",
		},
		//		{
		//			name:  "test1 1+2",
		//			left:  aoc.SVFromString("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]"),
		//			right: aoc.SVFromString("[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"),
		//			want:  aoc.SVFromString("[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]"),
		//		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			l := aoc.NewSE(c.left)
			r := aoc.NewSE(c.right)
			got := l.Add(r)
			want := aoc.NewSE(c.want)
			if !want.Equal(got) {
				t.Fatalf("Expected %v, got %v", want, got)
			}
		})
	}
}

func TestSnailReduction(t *testing.T) {
	cases := []struct {
		name string
		in   string
		want string
	}{
		{name: "explodes to right",
			in:   "[[[[[9,8],1],2],3],4]",
			want: "[[[[0,9],2],3],4]",
		},
		{name: "explodes to left",
			in:   "[7,[6,[5,[4,[3,2]]]]]",
			want: "[7,[6,[5,[7,0]]]]",
		},
		{name: "explodes to left and right",
			in:   "[[6,[5,[4,[3,2]]]],1]",
			want: "[[6,[5,[7,0]]],3]",
		},
		{name: "explodes recursively",
			in:   "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]",
			want: "[[3,[2,[8,0]]],[9,[5,[7,0]]]]",
		},
		{name: "simple Split uneven",
			in:   "[15,0]",
			want: "[[7,8],0]",
		},
		{name: "simple Split even",
			in:   "[16,0]",
			want: "[[8,8],0]",
		},
		{name: "recursive Split",
			in:   "[13, 18]",
			want: "[[6,7],[9,9]]",
		},
		{
			name: "explosion and Split recursive",
			in:   "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]",
			want: "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]",
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			in := aoc.NewSE(c.in)
			got := in.Reduce()
			want := aoc.NewSE(c.want)
			if !want.Equal(got) {
				t.Fatalf("Expected %v, got %v", want, got)
			}
		})
	}
}

func TestSnailMultiAdd(t *testing.T) {
	cases := []struct {
		name string
		in   []string
		want string
	}{
		{name: "adding without reduction",
			in:   []string{"[1,1]", "[2,2]", "[3,3]", "[4,4]"},
			want: "[[[[1,1],[2,2]],[3,3]],[4,4]]",
		},
		{name: "adding with explosion",
			in: []string{
				"[[[[1,1],[2,2]],[3,3]],[4,4]]",
				"[5,5]",
			},
			want: "[[[[3,0],[5,3]],[4,4]],[5,5]]",
		},
		{name: "adding example",
			in: []string{
				"[[[[3,0],[5,3]],[4,4]],[5,5]]",
				"[6,6]",
			},
			want: "[[[[5,0],[7,4]],[5,5]],[6,6]]",
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := aoc.NewSE(c.in[0])
			for _, v := range c.in[1:] {
				got = got.Add(aoc.NewSE(v))
			}
			want := aoc.NewSE(c.want)
			if !want.Equal(got) {
				t.Fatalf("Expected %v, got %v", want, got)
			}
		})
	}
}

func TestSnailExample(t *testing.T) {
	cases := []struct {
		name string
		in   string
		want string
	}{
		{name: "test1",
			in: `[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]`,
			want: "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]",
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := aoc.SnailSum(c.in)
			want := aoc.NewSE(c.want)
			if !want.Equal(got) {
				t.Fatalf("Expected %v, got %v", want, got)
			}
		})
	}
}

func TestSnailMagnitude(t *testing.T) {
	cases := []struct {
		name string
		in   string
		want int
	}{
		{name: "one pair",
			in:   "[9,1]",
			want: 29,
		},
		{name: "one pair 2",
			in:   "[1,9]",
			want: 21,
		},
		{name: "nested pairs",
			in:   "[[9,1],[1,9]]",
			want: 129,
		},
		{name: "nested pairs",
			in:   "[[[9,1], 3],[4, [1,9]]]",
			want: 387,
		},
		{name: "ex1",
			in:   "[[1,2],[[3,4],5]]",
			want: 143,
		},
		{name: "ex2",
			in:   "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]",
			want: 1384,
		},
		{name: "ex3",
			in:   "[[[[1,1],[2,2]],[3,3]],[4,4]]",
			want: 445,
		},
		{name: "ex4",
			in:   "[[[[3,0],[5,3]],[4,4]],[5,5]]",
			want: 791,
		},
		{name: "ex5",
			in:   "[[[[5,0],[7,4]],[5,5]],[6,6]]",
			want: 1137,
		},
		{name: "ex6",
			in:   "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]",
			want: 3488,
		},
		{name: "test",
			in:   "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]",
			want: 4140,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := aoc.NewSE(c.in).Magnitude()
			if c.want != got {
				t.Fatalf("Expected %v, got %v", c.want, got)
			}
		})
	}
}
