package main

import (
	"fmt"
	"math"
	"strings"
)

func strSoftMatch(a, b string) bool {
	if len(a) != len(b) {
		return false
	}
	for _, r := range []rune(a) {
		if !strings.ContainsRune(b, r) {
			return false
		}
	}
	return true
}

func determineNumbers(s string) []string {
	n069 := make([]string, 0, 3)
	n235 := make([]string, 0, 3)

	n := make([]string, 10)
	for _, v := range strings.Split(s, " ") {
		switch len(v) {
		case 2:
			n[1] = v
		case 3:
			n[7] = v
		case 4:
			n[4] = v
		case 7:
			n[8] = v
		case 5:
			n235 = append(n235, v)
		case 6:
			n069 = append(n069, v)
		}
	}

	for _, v := range n069 {
		// 0 and 9 completely overlap 1, but 6 doesn't
		if len(strDiff(n[1], v)) > 0 {
			n[6] = v
		} else {
			// 9 completely overlaps 4, while 0 doesn't
			if len(strDiff(n[4], v)) > 0 {
				n[0] = v
			} else {
				n[9] = v
			}
		}
	}

	for _, v := range n235 {
		// 3 completely covers 1
		if len(strDiff(n[1], v)) == 0 {
			n[3] = v
		} else {
			// 9 completely covers 5, but not 2
			if len(strDiff(v, n[9])) == 0 {
				n[5] = v
			} else {
				n[2] = v
			}
		}
	}
	return n
}

func decodeLine(l string) int {
	parts := strings.Split(l, "|")
	nums := determineNumbers(parts[0])
	val := 0
	ansVals := strings.Split(parts[1], " ")
	numLen := len(ansVals)
	for vi, v := range ansVals {
		for ni, n := range nums {
			if strSoftMatch(n, v) {
				val = val + ni*int(math.Pow(10, float64(numLen-vi-1)))
				break
			}
		}
	}
	return val
}

func decodeAndSum(data string) int {
	lines := strings.Split(data, "\n")
	t := 0
	for _, line := range lines {
		lv := decodeLine(line)
		t = t + lv
	}
	return t
}

func day8part2(data string) {
	fmt.Println("Day 8 Part 2")
	fmt.Println("Answer:", decodeAndSum(data))
}

func countValues(s string) int {
	nums := strings.Split(strings.TrimSpace(s), " ")
	c := 0
	for _, num := range nums {
		switch len(num) {
		case 2, 3, 4, 7:
			c++
		}
	}
	return c
}

func countUnique(data string) int {
	lines := strings.Split(data, "\n")
	c := 0
	for _, line := range lines {
		parts := strings.Split(line, "|")
		c = c + countValues(parts[1])
	}
	return c
}

func day8part1(data string) {
	fmt.Println("Day 8 Part 1")
	fmt.Println("Answer:", countUnique(data))
}
