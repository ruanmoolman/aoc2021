package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// SE is a snail expression
type SE struct {
	Values []SV
}

func (s *SE) String() string {
	sl := make([]string, 0)
	for _, v := range s.Values {
		sl = append(sl, fmt.Sprint(v))
	}
	return "[" + strings.Join(sl, " ") + "]"
}

func (s *SE) Equal(o *SE) bool {
	if len(s.Values) != len(o.Values) {
		return false
	}
	for i := 0; i < len(s.Values); i++ {
		if !s.Values[i].Equal(o.Values[i]) {
			return false
		}
	}
	return true
}

func (s *SE) Append(v SV) {
	s.Values = append(s.Values, v)
}

func (s *SE) Add(o *SE) *SE {
	nse := &SE{}
	for _, v := range s.Values {
		v.Depth++
		nse.Append(v)
	}
	for _, v := range o.Values {
		v.Depth++
		nse.Append(v)
	}
	return nse.Reduce()
}

func (s *SE) firstAtDepth(d int) int {
	for i := 0; i < len(s.Values); i++ {
		if s.Values[i].Depth >= d {
			return i
		}
	}
	return -1
}

func (s *SE) Reduce() *SE {
	ref := s
	red := s.reduce()
	for !ref.Equal(red) {
		ref = red
		red = ref.reduce()
	}
	return red
}

func (s *SE) reduce() *SE {
	// EXPLODE
	i := s.firstAtDepth(5)
	if i >= 0 {
		left := s.Values[i]
		right := s.Values[i+1]

		nse := &SE{}
		for c := 0; c < i; c++ {
			nse.Values = append(nse.Values, s.Values[c])
		}
		nse.Values = append(nse.Values, SV{0, left.Depth - 1})
		for c := i + 2; c < len(s.Values); c++ {
			nse.Values = append(nse.Values, s.Values[c])
		}
		if i > 0 {
			nse.Values[i-1].Value = nse.Values[i-1].Value + left.Value
		}
		if i+1 < len(nse.Values) {
			nse.Values[i+1].Value = nse.Values[i+1].Value + right.Value
		}
		return nse
	}
	// SPLIT
	// first above 9
	splitIdx := -1
	for i = 0; i < len(s.Values); i++ {
		if s.Values[i].Value > 9 {
			splitIdx = i
			break
		}
	}
	if splitIdx >= 0 {
		nse := &SE{}
		for c := 0; c < splitIdx; c++ {
			nse.Values = append(nse.Values, s.Values[c])
		}
		half := s.Values[splitIdx].Value / 2
		depth := s.Values[splitIdx].Depth + 1
		nse.Values = append(nse.Values, SV{half, depth})
		nse.Values = append(nse.Values, SV{s.Values[splitIdx].Value - half, depth})
		for c := splitIdx + 1; c < len(s.Values); c++ {
			nse.Values = append(nse.Values, s.Values[c])
		}
		return nse
	}
	return s
}

var numberRe = regexp.MustCompile(`^\d+`)

// NewSE creates an SE instance from a string representation
func NewSE(s string) *SE {
	se := &SE{}
	i := 0
	d := 0
	for {
		if i >= len(s) {
			break
		}
		if s[i] == '[' {
			d++
		} else if s[i] == ']' {
			d--
		} else {
			m := string(numberRe.Find([]byte(s[i:])))
			if m != "" {
				v, err := strconv.Atoi(m)
				if err != nil {
					panic(err)
				}
				se.Append(SV{v, d})
				i = i + len(m)
				continue
			}
		}
		i++
	}
	return se
}

// SV is a single snail value
type SV struct {
	Value, Depth int
}

func (s SV) String() string {
	return fmt.Sprintf("%v(%v)", s.Value, s.Depth)
}

func (s SV) Equal(o SV) bool {
	return s.Value == o.Value && s.Depth == o.Depth
}

func (s *SE) Magnitude() int {
	nse := s.Reduce()
	for d := 4; d > 0; d-- {
		oldS := nse
		// reset collector
		nse = &SE{}
		for i := 0; i < len(oldS.Values); i++ {
			val := oldS.Values[i]
			if val.Depth < d {
				nse.Values = append(nse.Values, val)
			} else {
				nse.Values = append(nse.Values,
					SV{3*val.Value + 2*oldS.Values[i+1].Value,
						val.Depth - 1})
				i++
			}
		}
	}
	return nse.Values[0].Value
}

func SnailSum(data string) *SE {
	ls := strings.Split(data, "\n")
	se := NewSE(ls[0])
	for i, v := range ls[1:] {
		fmt.Println(i+1, v)
		se = se.Add(NewSE(v))
	}
	return se
}

func day18part1(data string) {
	fmt.Println("Day 18 Part 1")
	sv := SnailSum(data)
	fmt.Println(sv)
	fmt.Println("Answer:", sv.Magnitude())
	fmt.Println("Test: 4140")
}

func day18part2(data string) {
	fmt.Println("Day 18 Part 2")
	ls := strings.Split(data, "\n")
	bestMag := 0
	for i := 0; i < len(ls); i++ {
		for j := 0; j < len(ls); j++ {
			a := NewSE(ls[i])
			b := NewSE(ls[j])
			mag := a.Add(b).Magnitude()
			if mag > bestMag {
				fmt.Printf("Best for %v+%v: %v\n", i, j, mag)
				bestMag = mag
			}
		}
	}
	fmt.Println("Answer:", bestMag)
	fmt.Println("Test: 3993")
}
