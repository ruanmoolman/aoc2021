package main

import (
	"aoc2021/grid"
	"fmt"
	"strings"
)

type PointPath []grid.Point

func (sl *PointPath) String() string {
	strs := make([]string, 0, len(*sl))
	for _, p := range *sl {
		strs = append(strs, fmt.Sprint(p))
	}
	return strings.Join(strs, ",")
}

func NewPointPath(s ...grid.Point) *PointPath {
	nsl := PointPath(s)
	return &nsl
}

func (sl *PointPath) Copy() *PointPath {
	np := &PointPath{}
	for _, n := range *sl {
		np.Append(n)
	}
	return np
}

func (sl *PointPath) Idx(i int) grid.Point {
	if i < 0 || i >= len(*sl) {
		return grid.Point{}
	}
	return []grid.Point(*sl)[i]
}

func (sl *PointPath) Append(n grid.Point) {
	*sl = append(*sl, n)
}

func (sl *PointPath) Last() grid.Point {
	return sl.Idx(len(*sl) - 1)
}

func (sl *PointPath) Contains(s grid.Point) bool {
	for _, lv := range *sl {
		if lv.Eq(s) {
			return true
		}
	}
	return false
}

func (sl *PointPath) ContainsCount(s grid.Point) int {
	c := 0
	for _, lv := range *sl {
		if lv.Eq(s) {
			c++
		}
	}
	return c
}
