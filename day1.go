package main

import (
	"fmt"
	"strings"
)

func dataToList(d string) []int {
	ls := strings.Split(d, "\n")
	ls = filterStrings(ls, func(s string) bool { return s != "" })
	is, err := toInts(ls)
	if err != nil {
		panic(err)
	}
	return is
}

func countIncreases(vs []int) int {
	var c int
	for i := 0; i < len(vs)-1; i++ {
		if vs[i] < vs[i+1] {
			c++
		}
	}
	return c
}

func day1part1(data string) {
	fmt.Println("Day 1 Part 1")
	values := dataToList(data)
	fmt.Println("Data:", values)
	fmt.Println("Number of increases:", countIncreases(values))
}

func movingSum(is []int, ws int) []int {
	n := len(is) - ws + 1
	nis := make([]int, n)
	for i := 0; i < n; i++ {
		nis[i] = sum(is[i : i+ws])
	}
	return nis
}

func day1part2(data string) {
	fmt.Println("Day 1 Part 2")
	vals := dataToList(data)
	movingSumVals := movingSum(vals, 3)
	fmt.Println("Data:", vals)
	fmt.Println("Processed Data:", movingSumVals)
	fmt.Println("Number of increases:", countIncreases(movingSumVals))
}
