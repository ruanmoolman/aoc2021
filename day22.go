package main

import (
	"fmt"
	"regexp"
	"sort"
)

// Cuboid is a cube [XMin, XMax, YMin, YMax, ZMin, ZMax]
type Cuboid [6]int

func (c *Cuboid) Volume() int {
	v := 1
	for i := 0; i < 3; i++ {
		v = v * (c[i*2+1] - c[i*2] + 1)
	}
	return v
}

func (c *Cuboid) Equal(o *Cuboid) bool {
	for i := 0; i < len(c); i++ {
		if c[i] != o[i] {
			return false
		}
	}
	return true
}

func (c *Cuboid) Scale(v int) *Cuboid {
	for i := 0; i < len(c); i++ {
		c[i] = c[i] * v
	}
	return c
}

func (c *Cuboid) Translate(o [3]int) *Cuboid {
	for i := 0; i < len(o); i++ {
		c[i*2] = c[i*2] + o[i]
		c[i*2+1] = c[i*2+1] + o[i]
	}
	return c
}

func (c *Cuboid) String() string {
	return fmt.Sprint(*c)
}

func (c *Cuboid) Contains(x, y, z int) bool {
	return x >= c[0] && x <= c[1] &&
		y >= c[2] && y <= c[3] &&
		z >= c[4] && z <= c[5]
}

func (c *Cuboid) Copy() *Cuboid {
	n := &Cuboid{}
	for i := 0; i < len(c); i++ {
		n[i] = c[i]
	}
	return n
}

func (c *Cuboid) Overlap(o *Cuboid) *Cuboid {
	r := &Cuboid{}
	for i := 0; i < 3; i++ {
		iMin, iMax := i*2, i*2+1
		r[iMin] = max(c[iMin], o[iMin])
		r[iMax] = min(c[iMax], o[iMax])
		if r[iMax] < r[iMin] {
			return nil
		}
	}
	return r
}

func (c *Cuboid) Split(axis, low, high int) (res []*Cuboid) {
	minI, maxI := axis*2, axis*2+1
	minV, maxV := c[minI], c[maxI]+1
	values := []int{minV, maxV}

	if low > minV && low < maxV {
		values = append(values, low)
	}
	high++ // adjust high we can treat it as the upper range, but not included
	if high > minV && high < maxV {
		values = append(values, high)
	}
	sort.Ints(values)
	for i := 0; i < len(values)-1; i++ {
		nc := c.Copy()
		nc[minI] = values[i]
		nc[maxI] = values[i+1] - 1
		res = append(res, nc)
	}
	return
}

type CuboidStep struct {
	onOff  bool
	cuboid *Cuboid
}

func (c *CuboidStep) String() string {
	return fmt.Sprint(*c)
}

var cuboidStepRe = regexp.MustCompile(`(on|off) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)`)

func parseCuboidSteps(s string) []*CuboidStep {
	ms := cuboidStepRe.FindAllStringSubmatch(s, -1)
	css := make([]*CuboidStep, len(ms))
	for i := 0; i < len(ms); i++ {
		m := ms[i]
		onOff := false
		if m[1] == "on" {
			onOff = true
		}
		css[i] = &CuboidStep{onOff,
			&Cuboid{mustAtoi(m[2]), mustAtoi(m[3]),
				mustAtoi(m[4]), mustAtoi(m[5]),
				mustAtoi(m[6]), mustAtoi(m[7])}}
	}
	return css
}

func (c *Cuboid) keyPoints(axis, low, high int) (values []int) {
	minI, maxI := axis*2, axis*2+1
	minV, maxV := c[minI], c[maxI]+1
	values = []int{minV, maxV}
	if low > minV && low < maxV {
		values = append(values, low)
	}
	high++ // adjust high we can treat it as the upper range, but not included
	if high > minV && high < maxV {
		values = append(values, high)
	}
	sort.Ints(values)
	return
}

func filterCuboids(cs []*Cuboid, filterFn func(*Cuboid) bool) []*Cuboid {
	c := make([]*Cuboid, 0, len(cs))
	for _, cube := range cs {
		if filterFn(cube) {
			c = append(c, cube)
		}
	}
	return c
}

func RemoveOverlapping(c, o *Cuboid) []*Cuboid {
	cubes := []*Cuboid{c}
	for ax := 0; ax < 3; ax++ {
		iMin, iMax := ax*2, ax*2+1
		newCubes := make([]*Cuboid, 0)
		for _, cube := range cubes {
			newCubes = append(newCubes, cube.Split(ax, o[iMin], o[iMax])...)
		}
		cubes = newCubes
	}
	cubes = filterCuboids(cubes, func(c *Cuboid) bool {
		return !c.Equal(o)
	})
	return cubes
}

func processCuboidSteps(steps []*CuboidStep) []*Cuboid {
	cubes := make([]*Cuboid, 0)
	for si, step := range steps {
		if si == 0 {
			cubes = append(cubes, step.cuboid)
			continue
		}
		allCubes := make([]*Cuboid, 0, len(cubes))
		for _, cube := range cubes {
			o := cube.Overlap(step.cuboid)
			if o != nil {
				allCubes = append(allCubes, RemoveOverlapping(cube, o)...)
			} else {
				allCubes = append(allCubes, cube)
			}
		}
		if step.onOff {
			allCubes = append(allCubes, step.cuboid)
		}
		cubes = allCubes
	}
	return cubes
}

func sumCuboidsVolume(cubes []*Cuboid) (c int) {
	for _, cube := range cubes {
		c = c + cube.Volume()
	}
	return
}

const CuboidLimit = 50

func simpleCount(steps []*CuboidStep) (c int) {
	for x := -CuboidLimit; x <= CuboidLimit; x++ {
		for y := -CuboidLimit; y <= CuboidLimit; y++ {
		zLoop:
			for z := -CuboidLimit; z <= CuboidLimit; z++ {
				for i := len(steps) - 1; i >= 0; i-- {
					if steps[i].cuboid.Contains(x, y, z) {
						if steps[i].onOff {
							c++
						}
						continue zLoop
					}
				}
			}
		}
	}
	return
}

func day22part1(data string) {
	fmt.Println("Day 22 Part 1")
	cuboidSteps := parseCuboidSteps(data)
	cubes := processCuboidSteps(cuboidSteps)
	clippedCubes := make([]*Cuboid, 0)
	limitCube := &Cuboid{
		-CuboidLimit, CuboidLimit,
		-CuboidLimit, CuboidLimit,
		-CuboidLimit, CuboidLimit}
	for _, c := range cubes {
		o := limitCube.Overlap(c)
		if o != nil {
			clippedCubes = append(clippedCubes, o)
		}
	}
	fmt.Println("Answer (simple):", simpleCount(cuboidSteps))
	fmt.Println("Answer:", sumCuboidsVolume(clippedCubes))
	fmt.Println("Test 1: 590784")
	fmt.Println("Puzzle: 615869")
}

func day22part2(data string) {
	fmt.Println("Day 22 Part 2")
	cuboidSteps := parseCuboidSteps(data)
	cubes := processCuboidSteps(cuboidSteps)
	fmt.Println("Answer:", sumCuboidsVolume(cubes))
	fmt.Println("Test 1: 39769202357779")
	fmt.Println("Test 2: 2758514936282235")
	fmt.Println("Puzzle: 1323862415207825")
}
