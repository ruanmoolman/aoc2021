package main_test

import (
	aoc "aoc2021"
	"fmt"
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

func TestDay25(t *testing.T) {
	cases := []struct {
		name      string
		in        string
		want      string
		wantCount int
	}{
		{"move east", ".>.", "..>", 1},
		{"wraps east", "..>", ">..", 1},
		{"east blocked", ".>>.", ".>.>", 1},
		{"move south", ".\nv\n.", ".\n.\nv", 1},
		{"wraps south", ".\n.\nv", "v\n.\n.", 1},
		{"wraps south", ".\nv\nv\n.", ".\nv\n.\nv", 1},
		{"tick", `
>v.
.>.
v.>`, `
>..
.v>
v.>`, 2},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			want := strings.Trim(c.want, "\n")
			g := aoc.NewSCGrid(c.in)
			gotCount := g.Tick()
			require.Equal(t, want, fmt.Sprint(g))
			require.Equal(t, c.wantCount, gotCount)
		})
	}
}
